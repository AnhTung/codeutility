﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeUtility;

namespace Demo_EncodeDecode
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        private void button_EncodeUrl_Click(object sender, EventArgs e)
        {
            string input = textBox_Input.Text.Trim();

            string output = SecurityUtility.UrlEncode(input);

            textBox_Output.Text = output;
        }

        private void button_DecodeUrl_Click(object sender, EventArgs e)
        {
            string output = textBox_Output.Text.Trim();

            string input = SecurityUtility.UrlDecode(output);

            textBox_Input.Text = input;
        }

        private void button_EncodeHtml_Click(object sender, EventArgs e)
        {
            string input = textBox_Input.Text.Trim();

            string output = SecurityUtility.HtmlEncode(input);

            textBox_Output.Text = output;
        }

        private void button_DecodeHtml_Click(object sender, EventArgs e)
        {
            string output = textBox_Output.Text.Trim();

            string input = SecurityUtility.HtmlDecode(output);

            textBox_Input.Text = input;
        }

        private void button_MD5_Click(object sender, EventArgs e)
        {
            string input = textBox_Input.Text.Trim();
            string salt = "@!vbd";
            string output = SecurityUtility.MD5Encrypt(input, salt);

            textBox_Output.Text = output;
        }

        private void button_SHA256_Click(object sender, EventArgs e)
        {
            string input = textBox_Input.Text.Trim();
            string salt = "@!vbd";
            string output = SecurityUtility.SHA256Encrypt(input, salt);

            textBox_Output.Text = output;
        }

        private void button_SaltGenerate_Click(object sender, EventArgs e)
        {
            string original = textBox_Input.Text;
            string salt = "abc";
            string enscript = SecurityUtility.AES256Encrypt(original, "123", salt);
            string descript = SecurityUtility.AES256Decrypt(enscript, "123", salt);

            textBox_Output.Text = string.Format("enscript{2}{0}{2}{2}descript{2}{1}", enscript, descript, Environment.NewLine);
        }

        private void button_AES256_Click(object sender, EventArgs e)
        {
            string input = textBox_Input.Text;

            //Mã hóa
            string encryptValue = SecurityUtility.AES256Encrypt(input, "123456", "@abc");
            //Giải mã
            string decryptValue = SecurityUtility.AES256Decrypt(encryptValue, "123456", "@abc");

            //Xuất lên
            string display = "encryptValue:{0}{1}{0}{0}decryptValue:{0}{2}";
            textBox_Output.Text = string.Format(display, Environment.NewLine, encryptValue, decryptValue);
        }

        private void button_Random_Click(object sender, EventArgs e)
        {
            string template = "Số ngẫu nhiên: {0}\r\n";
            template += "Số ngẫu nhiên (min=0; max=999): {1}\r\n";
            template += "Số ngẫu nhiên (min=111; max=999): {2}\r\n\r\n";

            template += "Số thực ngẫu nhiên: {3}\r\n";
            template += "Số thực ngẫu nhiên (min=0; max=999): {4}\r\n";
            template += "Số thực ngẫu nhiên (min=111; max=999): {5}\r\n\r\n";

            template += "Chuỗi ngẫu nhiên: {6}\r\n";
            template += "Chuỗi ngẫu nhiên(Chỉ có chữ): {7}\r\n";
            template += "Chuỗi ngẫu nhiên(Chỉ có số): {8}\r\n";
            template += "Chuỗi ngẫu nhiên(Chỉ có ký tự ĐB): {9}\r\n";
            template += "Chuỗi ngẫu nhiên(Chữ + số): {10}\r\n";
            template += "Chuỗi ngẫu nhiên(Số + ký tự ĐB): {11}\r\n\r\n";

            template += "Mật mã ngẫu nhiên: {12}\r\n";
            template += "Mật mã ngẫu nhiên: {13}\r\n\r";
            template += "Mật mã ngẫu nhiên: {14}\r\n\r\n";

            template += "ID không trùng lắp: {15}\r\n\r\n";

            int number0 = RandomUtility.RandomInt();
            int number1 = RandomUtility.RandomInt(999);
            int number2 = RandomUtility.RandomInt(111, 999);

            double number3 = RandomUtility.RandomDouble();
            double number4 = RandomUtility.RandomDouble(999);
            double number5 = RandomUtility.RandomDouble(111, 999);

            string string1 = RandomUtility.RandomString(5,5);
            string string2 = RandomUtility.RandomString(4, 10, true, false, false);
            string string3 = RandomUtility.RandomString(4, 10, false, true, false);
            string string4 = RandomUtility.RandomString(4, 10, false, false, true);
            string string5 = RandomUtility.RandomString(4, 10, true, true, false);
            string string6 = RandomUtility.RandomString(4, 10, false, true, true);

            string password1 = RandomUtility.RandomPassword(10);
            string password2 = RandomUtility.RandomPassword(10);
            string password3 = RandomUtility.RandomPassword(5,5);

            string id = RandomUtility.RandomID();

            textBox_Output.Text = string.Format(template, number0, number1, number2, number3, number4, number5, string1, string2, string3, string4, string5, string6, password1, password2, password3, id);
        }
    }
}
