﻿namespace Demo_EncodeDecode
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Input = new System.Windows.Forms.TextBox();
            this.textBox_Output = new System.Windows.Forms.TextBox();
            this.button_EncodeUrl = new System.Windows.Forms.Button();
            this.button_DecodeUrl = new System.Windows.Forms.Button();
            this.button_EncodeHtml = new System.Windows.Forms.Button();
            this.button_DecodeHtml = new System.Windows.Forms.Button();
            this.button_MD5 = new System.Windows.Forms.Button();
            this.button_SHA256 = new System.Windows.Forms.Button();
            this.button_AES256 = new System.Windows.Forms.Button();
            this.button_Random = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_Input
            // 
            this.textBox_Input.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Input.Location = new System.Drawing.Point(2, 2);
            this.textBox_Input.Multiline = true;
            this.textBox_Input.Name = "textBox_Input";
            this.textBox_Input.Size = new System.Drawing.Size(480, 120);
            this.textBox_Input.TabIndex = 0;
            // 
            // textBox_Output
            // 
            this.textBox_Output.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Output.Location = new System.Drawing.Point(2, 128);
            this.textBox_Output.Multiline = true;
            this.textBox_Output.Name = "textBox_Output";
            this.textBox_Output.Size = new System.Drawing.Size(480, 274);
            this.textBox_Output.TabIndex = 0;
            // 
            // button_EncodeUrl
            // 
            this.button_EncodeUrl.Location = new System.Drawing.Point(2, 408);
            this.button_EncodeUrl.Name = "button_EncodeUrl";
            this.button_EncodeUrl.Size = new System.Drawing.Size(75, 23);
            this.button_EncodeUrl.TabIndex = 1;
            this.button_EncodeUrl.Text = "EncodeUrl";
            this.button_EncodeUrl.UseVisualStyleBackColor = true;
            this.button_EncodeUrl.Click += new System.EventHandler(this.button_EncodeUrl_Click);
            // 
            // button_DecodeUrl
            // 
            this.button_DecodeUrl.Location = new System.Drawing.Point(131, 408);
            this.button_DecodeUrl.Name = "button_DecodeUrl";
            this.button_DecodeUrl.Size = new System.Drawing.Size(75, 23);
            this.button_DecodeUrl.TabIndex = 1;
            this.button_DecodeUrl.Text = "DecodeUrl";
            this.button_DecodeUrl.UseVisualStyleBackColor = true;
            this.button_DecodeUrl.Click += new System.EventHandler(this.button_DecodeUrl_Click);
            // 
            // button_EncodeHtml
            // 
            this.button_EncodeHtml.Location = new System.Drawing.Point(272, 408);
            this.button_EncodeHtml.Name = "button_EncodeHtml";
            this.button_EncodeHtml.Size = new System.Drawing.Size(75, 23);
            this.button_EncodeHtml.TabIndex = 1;
            this.button_EncodeHtml.Text = "EncodeHtml";
            this.button_EncodeHtml.UseVisualStyleBackColor = true;
            this.button_EncodeHtml.Click += new System.EventHandler(this.button_EncodeHtml_Click);
            // 
            // button_DecodeHtml
            // 
            this.button_DecodeHtml.Location = new System.Drawing.Point(407, 408);
            this.button_DecodeHtml.Name = "button_DecodeHtml";
            this.button_DecodeHtml.Size = new System.Drawing.Size(75, 23);
            this.button_DecodeHtml.TabIndex = 1;
            this.button_DecodeHtml.Text = "DecodeHtml";
            this.button_DecodeHtml.UseVisualStyleBackColor = true;
            this.button_DecodeHtml.Click += new System.EventHandler(this.button_DecodeHtml_Click);
            // 
            // button_MD5
            // 
            this.button_MD5.Location = new System.Drawing.Point(2, 437);
            this.button_MD5.Name = "button_MD5";
            this.button_MD5.Size = new System.Drawing.Size(75, 23);
            this.button_MD5.TabIndex = 1;
            this.button_MD5.Text = "MD5";
            this.button_MD5.UseVisualStyleBackColor = true;
            this.button_MD5.Click += new System.EventHandler(this.button_MD5_Click);
            // 
            // button_SHA256
            // 
            this.button_SHA256.Location = new System.Drawing.Point(131, 436);
            this.button_SHA256.Name = "button_SHA256";
            this.button_SHA256.Size = new System.Drawing.Size(75, 23);
            this.button_SHA256.TabIndex = 1;
            this.button_SHA256.Text = "SHA256";
            this.button_SHA256.UseVisualStyleBackColor = true;
            this.button_SHA256.Click += new System.EventHandler(this.button_SHA256_Click);
            // 
            // button_AES256
            // 
            this.button_AES256.Location = new System.Drawing.Point(272, 436);
            this.button_AES256.Name = "button_AES256";
            this.button_AES256.Size = new System.Drawing.Size(75, 23);
            this.button_AES256.TabIndex = 3;
            this.button_AES256.Text = "AES256";
            this.button_AES256.UseVisualStyleBackColor = true;
            this.button_AES256.Click += new System.EventHandler(this.button_AES256_Click);
            // 
            // button_Random
            // 
            this.button_Random.Location = new System.Drawing.Point(407, 436);
            this.button_Random.Name = "button_Random";
            this.button_Random.Size = new System.Drawing.Size(75, 23);
            this.button_Random.TabIndex = 3;
            this.button_Random.Text = "Random";
            this.button_Random.UseVisualStyleBackColor = true;
            this.button_Random.Click += new System.EventHandler(this.button_Random_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.button_Random);
            this.Controls.Add(this.button_AES256);
            this.Controls.Add(this.button_SHA256);
            this.Controls.Add(this.button_MD5);
            this.Controls.Add(this.button_DecodeHtml);
            this.Controls.Add(this.button_EncodeHtml);
            this.Controls.Add(this.button_DecodeUrl);
            this.Controls.Add(this.button_EncodeUrl);
            this.Controls.Add(this.textBox_Output);
            this.Controls.Add(this.textBox_Input);
            this.Name = "Form_Main";
            this.Text = "Encode Decode Demo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Input;
        private System.Windows.Forms.TextBox textBox_Output;
        private System.Windows.Forms.Button button_EncodeUrl;
        private System.Windows.Forms.Button button_DecodeUrl;
        private System.Windows.Forms.Button button_EncodeHtml;
        private System.Windows.Forms.Button button_DecodeHtml;
        private System.Windows.Forms.Button button_MD5;
        private System.Windows.Forms.Button button_SHA256;
        private System.Windows.Forms.Button button_AES256;
        private System.Windows.Forms.Button button_Random;
    }
}

