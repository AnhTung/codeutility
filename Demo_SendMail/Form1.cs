﻿using System;
using System.Windows.Forms;
using CodeUtility;
using System.IO;
using System.Collections.Generic;

namespace Demo_SendMail
{
    public partial class Form_Main : Form
    {
        string attachmentFile = string.Empty;

        public Form_Main()
        {
            InitializeComponent();
        }

        private void button_Send_Click(object sender, EventArgs e)
        {
            string to = textBox_To.Text.Trim();
            string cc = textBox_CC.Text.Trim();
            string bcc = textBox_BCC.Text.Trim();

            string subject = textBox_Subject.Text.Trim();
            string body = textBox_Body.Text.Trim();

            string from = "laptrinhvien2015@gmail.com";
            string password = "2015@VienLapTrinh";

            string host = "smtp.gmail.com";
            int port = 587;
            bool enableSSL = true;
            bool isBodyHtml = true;

            //Kiểm tra thông tin sẽ gửi
            if (to == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập địa chỉ email người nhận");
                textBox_To.Focus();
                return;
            }

            if (subject == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập chủ đề của email");
                textBox_Subject.Focus();
                return;
            }

            if (body == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập nội dung email");
                textBox_Body.Focus();
                return;
            }

            //Gọi hàm theo hàm tĩnh
            //CodeUtility.MailUtility.Send(from, password, host, port, enableSSL, to, subject, body, isBodyHtml);

            //Gọi hàm theo đối tượng
            MailUtility mail = new MailUtility();
            mail.Body = body;
            mail.EnableSSL = enableSSL;
            mail.From = from;
            mail.Host = host;
            mail.IsBodyHtml = isBodyHtml;
            mail.Password = password;
            mail.Port = port;
            mail.Subject = subject;
            mail.To = to;
            mail.CC = cc;
            mail.BCC = bcc;
            mail.AttachmentFile = attachmentFile;

            mail.Send();

            if(mail.LastException != null)
            {
                MessageBox.Show("Có lỗi: " + mail.LastException.Message);
                return;
            }

            MessageBox.Show("Đã gửi email");

        }

        private void button_Attachment_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;
            fileName = DialogUtility.FileDialog("Vui lòng chọn file đính kèm");

            if (fileName == string.Empty)
            {
                label_File.Text = "File đính kèm: ";
                attachmentFile = string.Empty;
                return;
            }

            attachmentFile = fileName;
            fileName = Path.GetFileName(fileName);
            label_File.Text = "File đính kèm: " + fileName;
        }

        private void button_SendTemplate_Click(object sender, EventArgs e)
        {
            //1. Hiện hộp thoại chọn file, lọc các loại file: txt, html, htm
            string templateUrl = DialogUtility.FileDialog("Vui lòng chọn file mẫu", "Text Files|*.txt|HTML Files|*.htm;*.html");

            //2. Kiểm tra file, nếu = rỗng thì báo lỗi và kết thúc
            if(templateUrl == string.Empty)
            {
                MessageBox.Show("Bạn chưa chọn file mẫu.");
                return;
            }

            //3. Định nghĩa 1 Dictionary chứa các tham số và giá trị
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("#email#", textBox_To.Text.Trim());
            param.Add("#subject#", textBox_Subject.Text.Trim());

            //4. Khai báo các biến lấy giá trị trên Form
            string to = textBox_To.Text.Trim();
            string cc = textBox_CC.Text.Trim();
            string bcc = textBox_BCC.Text.Trim();

            string subject = textBox_Subject.Text.Trim();

            string from = "laptrinhvien2015@gmail.com";
            string password = "2015@VienLapTrinh";

            string host = "smtp.gmail.com";
            int port = 587;
            bool enableSSL = true;
            bool isBodyHtml = true;

            //5. Kiểm tra thông tin sẽ gửi
            if (to == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập địa chỉ email người nhận");
                textBox_To.Focus();
                return;
            }

            if (subject == string.Empty)
            {
                MessageBox.Show("Vui lòng nhập chủ đề của email");
                textBox_Subject.Focus();
                return;
            }

            //Gọi hàm theo đối tượng
            MailUtility mail = new MailUtility();
            mail.EnableSSL = enableSSL;
            mail.From = from;
            mail.Host = host;
            mail.IsBodyHtml = isBodyHtml;
            mail.Password = password;
            mail.Port = port;
            mail.Subject = subject;
            mail.To = to;
            mail.CC = cc;
            mail.BCC = bcc;
            mail.AttachmentFile = attachmentFile;

            mail.SendWithTemplate(templateUrl, param);

            if (mail.LastException != null)
            {
                MessageBox.Show("Có lỗi: " + mail.LastException.Message);
                return;
            }

            MessageBox.Show("Đã gửi email");
        }
    }
}
