﻿namespace Demo_SendMail
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_To = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Subject = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_Body = new System.Windows.Forms.TextBox();
            this.button_Send = new System.Windows.Forms.Button();
            this.button_SendTemplate = new System.Windows.Forms.Button();
            this.button_Attachment = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_CC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_BCC = new System.Windows.Forms.TextBox();
            this.label_File = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Người nhận:";
            // 
            // textBox_To
            // 
            this.textBox_To.Location = new System.Drawing.Point(83, 6);
            this.textBox_To.Name = "textBox_To";
            this.textBox_To.Size = new System.Drawing.Size(389, 20);
            this.textBox_To.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Chủ đề:";
            // 
            // textBox_Subject
            // 
            this.textBox_Subject.Location = new System.Drawing.Point(83, 84);
            this.textBox_Subject.Name = "textBox_Subject";
            this.textBox_Subject.Size = new System.Drawing.Size(389, 20);
            this.textBox_Subject.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nội dung:";
            // 
            // textBox_Body
            // 
            this.textBox_Body.Location = new System.Drawing.Point(83, 109);
            this.textBox_Body.Multiline = true;
            this.textBox_Body.Name = "textBox_Body";
            this.textBox_Body.Size = new System.Drawing.Size(389, 259);
            this.textBox_Body.TabIndex = 4;
            // 
            // button_Send
            // 
            this.button_Send.Image = global::Demo_SendMail.Properties.Resources.Send;
            this.button_Send.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_Send.Location = new System.Drawing.Point(83, 406);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(61, 43);
            this.button_Send.TabIndex = 5;
            this.button_Send.Text = "Gửi";
            this.button_Send.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.button_Send_Click);
            // 
            // button_SendTemplate
            // 
            this.button_SendTemplate.Image = global::Demo_SendMail.Properties.Resources.template;
            this.button_SendTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_SendTemplate.Location = new System.Drawing.Point(158, 406);
            this.button_SendTemplate.Name = "button_SendTemplate";
            this.button_SendTemplate.Size = new System.Drawing.Size(103, 43);
            this.button_SendTemplate.TabIndex = 6;
            this.button_SendTemplate.Text = "Gửi theo mẫu";
            this.button_SendTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_SendTemplate.UseVisualStyleBackColor = true;
            this.button_SendTemplate.Click += new System.EventHandler(this.button_SendTemplate_Click);
            // 
            // button_Attachment
            // 
            this.button_Attachment.Image = global::Demo_SendMail.Properties.Resources.attachment;
            this.button_Attachment.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_Attachment.Location = new System.Drawing.Point(275, 406);
            this.button_Attachment.Name = "button_Attachment";
            this.button_Attachment.Size = new System.Drawing.Size(109, 43);
            this.button_Attachment.TabIndex = 7;
            this.button_Attachment.Text = "Đính kèm file";
            this.button_Attachment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_Attachment.UseVisualStyleBackColor = true;
            this.button_Attachment.Click += new System.EventHandler(this.button_Attachment_Click);
            // 
            // button_Close
            // 
            this.button_Close.Image = global::Demo_SendMail.Properties.Resources.exit;
            this.button_Close.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_Close.Location = new System.Drawing.Point(398, 406);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(74, 43);
            this.button_Close.TabIndex = 8;
            this.button_Close.Text = "Thoát";
            this.button_Close.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_Close.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "CC:";
            // 
            // textBox_CC
            // 
            this.textBox_CC.Location = new System.Drawing.Point(83, 32);
            this.textBox_CC.Name = "textBox_CC";
            this.textBox_CC.Size = new System.Drawing.Size(389, 20);
            this.textBox_CC.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "BCC:";
            // 
            // textBox_BCC
            // 
            this.textBox_BCC.Location = new System.Drawing.Point(83, 58);
            this.textBox_BCC.Name = "textBox_BCC";
            this.textBox_BCC.Size = new System.Drawing.Size(389, 20);
            this.textBox_BCC.TabIndex = 2;
            // 
            // label_File
            // 
            this.label_File.AutoSize = true;
            this.label_File.Location = new System.Drawing.Point(80, 381);
            this.label_File.Name = "label_File";
            this.label_File.Size = new System.Drawing.Size(75, 13);
            this.label_File.TabIndex = 0;
            this.label_File.Text = "File đính kèm:";
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.button_Attachment);
            this.Controls.Add(this.button_SendTemplate);
            this.Controls.Add(this.button_Send);
            this.Controls.Add(this.textBox_Body);
            this.Controls.Add(this.label_File);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_Subject);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_BCC);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_CC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_To);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "Chương trình gửi email";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_To;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Subject;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_Body;
        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.Button button_SendTemplate;
        private System.Windows.Forms.Button button_Attachment;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_CC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_BCC;
        private System.Windows.Forms.Label label_File;
    }
}

