﻿namespace Demo_StringConvert
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_ToUpperCase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_ToLowerCase = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_ToTitleCase = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_ToNoSign = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_ToUrlFormat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSubstr = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox_Input
            // 
            this.textBox_Input.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Input.Location = new System.Drawing.Point(2, 28);
            this.textBox_Input.Name = "textBox_Input";
            this.textBox_Input.Size = new System.Drawing.Size(479, 20);
            this.textBox_Input.TabIndex = 0;
            this.textBox_Input.Text = "Mỗi ngày tôi chọn một niềm vui!!!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input";
            // 
            // textBox_ToUpperCase
            // 
            this.textBox_ToUpperCase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ToUpperCase.Location = new System.Drawing.Point(2, 72);
            this.textBox_ToUpperCase.Name = "textBox_ToUpperCase";
            this.textBox_ToUpperCase.Size = new System.Drawing.Size(479, 20);
            this.textBox_ToUpperCase.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "To Upper Case (Kiểu In Hoa)";
            // 
            // textBox_ToLowerCase
            // 
            this.textBox_ToLowerCase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ToLowerCase.Location = new System.Drawing.Point(2, 118);
            this.textBox_ToLowerCase.Name = "textBox_ToLowerCase";
            this.textBox_ToLowerCase.Size = new System.Drawing.Size(479, 20);
            this.textBox_ToLowerCase.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-1, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "To Lower Case (Kiểu In Thường)";
            // 
            // textBox_ToTitleCase
            // 
            this.textBox_ToTitleCase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ToTitleCase.Location = new System.Drawing.Point(2, 165);
            this.textBox_ToTitleCase.Name = "textBox_ToTitleCase";
            this.textBox_ToTitleCase.Size = new System.Drawing.Size(479, 20);
            this.textBox_ToTitleCase.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-1, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "To Title Case (Kiểu Tiêu Đề)";
            // 
            // textBox_ToNoSign
            // 
            this.textBox_ToNoSign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ToNoSign.Location = new System.Drawing.Point(2, 216);
            this.textBox_ToNoSign.Name = "textBox_ToNoSign";
            this.textBox_ToNoSign.Size = new System.Drawing.Size(479, 20);
            this.textBox_ToNoSign.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(-1, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "To No Signs (Kiểu Không Dấu)";
            // 
            // textBox_ToUrlFormat
            // 
            this.textBox_ToUrlFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ToUrlFormat.Location = new System.Drawing.Point(2, 265);
            this.textBox_ToUrlFormat.Name = "textBox_ToUrlFormat";
            this.textBox_ToUrlFormat.Size = new System.Drawing.Size(479, 20);
            this.textBox_ToUrlFormat.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-1, 246);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "To URL Format (Kiểu URL)";
            // 
            // button_OK
            // 
            this.button_OK.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_OK.Location = new System.Drawing.Point(121, 350);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(241, 23);
            this.button_OK.TabIndex = 1;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-1, 294);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "SubString With Leng And Priority";
            // 
            // txtSubstr
            // 
            this.txtSubstr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubstr.Location = new System.Drawing.Point(2, 313);
            this.txtSubstr.Name = "txtSubstr";
            this.txtSubstr.Size = new System.Drawing.Size(479, 20);
            this.txtSubstr.TabIndex = 9;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 385);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSubstr);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_ToUrlFormat);
            this.Controls.Add(this.textBox_ToNoSign);
            this.Controls.Add(this.textBox_ToTitleCase);
            this.Controls.Add(this.textBox_ToLowerCase);
            this.Controls.Add(this.textBox_ToUpperCase);
            this.Controls.Add(this.textBox_Input);
            this.Name = "Form_Main";
            this.Text = "Chương Trinh Chuyển Đổi Chuỗi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_ToUpperCase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_ToLowerCase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_ToTitleCase;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_ToNoSign;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_ToUrlFormat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSubstr;
    }
}

