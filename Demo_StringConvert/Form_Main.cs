﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeUtility;

namespace Demo_StringConvert
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
			string input = textBox_Input.Text.Trim();

			textBox_ToLowerCase.Text = input.ToLowerCase();
			textBox_ToNoSign.Text = input.ToNoSings();
			textBox_ToTitleCase.Text = input.ToTitleCase();
			textBox_ToUpperCase.Text = input.ToUpperCase();
			textBox_ToUrlFormat.Text = input.ToUrlFormat();
            txtSubstr.Text = input.SubStringWithLength(45, false, StringCaseType.None, false);
		}

		private void Form_Main_Load(object sender, EventArgs e)
		{

		}
	}
}
