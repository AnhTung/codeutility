﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeUtility
{
    public class FolderUtility
    {
        /// <summary>
        /// Kiểm tra sự tồn tại của một folder tại url.
        /// Nếu folder có tồn tại: trả về true; ngược lại trả về false.
        /// Nếu url là null, empty hoặc không hợp lệ, kết quả trả về là false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần kiểm tra tồn tại.</param>
        /// <returns>Nếu folder có tồn tại: trả về true; ngược lại trả về false.</returns>
        public static bool IsExists(string url)
        {
            return Directory.Exists(url);
        }

        /// <summary>
        /// Tạo folder tại url.
        /// Nếu url có dạng đa cấp như: C:\User\Image\Demo. Thì các folder chưa có sẵn sẽ được tạo một cách lần lượt từ cấp lớn đến nhỏ. VD: User -> Image -> Demo
        /// Nếu folder được tạo ra thành công hoặc đã có sẵn thì trả về true; ngược lại trả về false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tạo mới.</param>
        /// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình tạo folder.</param>
        /// <returns>Nếu folder được tạo ra thành công hoặc đã có sẵn thì trả về true; ngược lại trả về false.</returns>
        public static bool CreateFolder(string url, ref Exception error)
        {
            //1. Kiểm tra nếu folder tại url đã tồn tại thì không cần tạo, trả về true
            if (IsExists(url))
            {
                return true;
            }

            try
            {
                //2. Tạo folder tại url
                Directory.CreateDirectory(url);
            }
            catch (Exception ex)
            {
                error = ex;
                return false;
            }

            //3. Trả về kết quả thành công
            return true;
        }

        /// <summary>
        /// Tính tổng dung lượng của một folder tại url. Đơn vị tính là kilobyte.
        /// Nếu url không tồn tại, trả về 0.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tính dung lượng.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về tổng dung lượng của một folder. Đơn vị tính là kilobyte.</returns>
        public static long GetSize(string url, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về 0
            if (!IsExists(url))
                return 0;

            //2. Liệt kê danh sách file nằm trong folder
            string[] fileUrls = Directory.GetFiles(url, "*.*", searchOption);

            //3. Tạo một biến để chứa dung lượng
            long size = 0;

            //4. Lặp qua từng file, cộng dồn dung lượng các file lại với nhau
            foreach (string fileUrl in fileUrls)
            {
                //Load file từ url
                FileInfo file = new FileInfo(fileUrl);
                //Cộng dồn dung lượng lại với nhau
                size += file.Length;
            }

            //5. Trả về kết quả
            return size;
        }

        /// <summary>
        /// Đếm số lượng file bên trong folder tại url.
        /// Nếu folder tại url không tồn tại, trả về 0.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần đếm số lượng file.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về số lượng file bên trong folder tại url.</returns>
        public static int CountFiles(string url, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về 0
            if (!IsExists(url))
                return 0;

            //2. Liệt kê danh sách file nằm trong folder
            string[] fileUrls = Directory.GetFiles(url, "*.*", searchOption);

            //3. Trả về số lượng file
            return fileUrls.Length;
        }

        /// <summary>
        /// Đếm số lượng folder con bên trong folder tại url. 
        /// Nếu folder tại url không tồn tại, trả về 0.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần đếm số lượng folder con.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về số lượng folder con bên trong folder tại url.</returns>
        public static int CountFolders(string url, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về 0
            if (!IsExists(url))
                return 0;

            //2. Liệt kê danh sách folder nằm trong folder đang xét
            string[] fileUrls = Directory.GetDirectories(url, "*.*", searchOption);

            //3. Trả về số lượng folder đếm được
            return fileUrls.Length;
        }

        /// <summary>
        /// Đếm số lượng file và folder con bên trong folder tại url. 
        /// Nếu folder tại url không tồn tại, trả về 0.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần đếm số lượng file + folder.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về số lượng file + folder con bên trong folder tại url.</returns>
        public static int CountItems(string url, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về 0
            if (!IsExists(url))
                return 0;

            //2. Đếm số lượng file
            int fileNumbers = CountFiles(url, searchOption);

            //3. Đếm số lượng folder
            int folderNumbers = CountFolders(url, searchOption);

            //4. Cộng tổng số lượng file và folder
            int itemNumbers = fileNumbers + folderNumbers;

            //5. Trả về kết quả
            return itemNumbers;
        }

        /// <summary>
        /// Di chuyển một folder từ url đến newUrl. Có bắt lỗi ngoại lệ (nếu có).
        /// Nếu di chuyển thành công: trả về true; ngược lại trả về false.
        /// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần chuyển đi. Ví dụ: C:\Images</param>
        /// <param name="newUrl">Đường dẫn đến folder cần chuyển đến. Ví dụ: C:\Document\Images</param>
        /// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình di chuyển folder.</param>
        /// <returns>Nếu di chuyển thành công: trả về true; ngược lại trả về false.</returns>
        public static bool Move(string url, string newUrl, ref Exception error)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về false
            if (!IsExists(url))
            {
                string message = string.Format("The folder with path is not exist: {0}", url);
                error = new Exception(message);
                return false;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            try
            {
                //3. Di chuyển folder sang newUrl
                folder.MoveTo(newUrl);
            }
            catch (Exception ex)
            {
                error = ex;
                return false;
            }

            //4. Trả về kết quả thành công
            return true;
        }

        /// <summary>
        /// Đổi tên một folder sang tên mới. Có bắt lỗi ngoại lệ (nếu có).
        /// Nếu đổi tên thành công: trả về true; ngược lại trả về false.
        /// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần đổi tên.</param>
        /// <param name="newName">Tên mới của folder.</param>
        /// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình đổi tên folder.</param>
        /// <returns>Nếu đổi tên thành công: trả về true; ngược lại trả về false.</returns>
        public static bool Rename(string url, string newName, ref Exception error)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về false
            if (!IsExists(url))
            {
                string message = string.Format("The folder with path is not exist: {0}", url);
                error = new Exception(message);
                return false;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            //3. Load folder cha của url
            DirectoryInfo parentFolder = folder.Parent;

            //4. Tạo url với tên mới
            string newUrl = Path.Combine(parentFolder.FullName, newName);

            //5. Di chuyển folder từ url sang newUrl
            return Move(url, newUrl, ref error);
        }

        /// <summary>
        /// Xóa một folder. Có bắt lỗi ngoại lệ (nếu có).
        /// Nếu xóa thành công hoặc folder không tồn tại: trả về true; ngược lại trả về false.
        /// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần xóa.</param>
        /// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình xóa folder.</param>
        /// <returns>Nếu xóa thành công hoặc folder không tồn tại: trả về true; ngược lại trả về false.</returns>
        public static bool Delete(string url, ref Exception error)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về true
            if (!IsExists(url))
            {
                return true;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            try
            {
                //3. Xóa folder
                folder.Delete(true);
            }
            catch (Exception ex)
            {
                error = ex;
                return false;
            }

            //4. Trả về kết quả thành công
            return true;
        }

        /// <summary>
        /// Xóa hết file và folder con bên trong folder tại url. Có bắt lỗi ngoại lệ (nếu có).
        /// Nếu xóa hết thành công: trả về true; ngược lại trả về false.
        /// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần xóa hết file và folder con bên trong.</param>
        /// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình xóa hết.</param>
        /// <returns>Nếu xóa thành công: trả về true; ngược lại trả về false.</returns>
        public static bool Empty(string url, ref Exception error)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về false
            if (!IsExists(url))
            {
                string message = string.Format("The folder with path is not exist: {0}", url);
                error = new Exception(message);
                return false;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            try
            {
                //3. Xóa tất cả file bên trong folder này
                foreach (FileInfo file in folder.GetFiles())
                {
                    file.Delete();
                }

                //4. Xóa tất cả folder bên trong folder này
                foreach (DirectoryInfo subFolder in folder.GetDirectories())
                {
                    subFolder.Delete(true);
                }
            }
            catch (Exception ex)
            {
                error = ex;
                return false;
            }

            //5. Trả về kết quả thành công
            return true;
        }

        /// <summary>
        /// Tìm kiếm danh sách folder con bên trong folder tại url.
        /// Trả về danh sách folder con tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm folder con bên trong.</param>
        /// <param name="searchPattern">
        /// Mẫu để tìm kiếm. 
        /// VD1:  image* : tìm tất cả các folder con có tên bắt đầu là image. 
        /// VD2: *image  : tìm tất cả các folder con có tên kết thúc là image
        /// VD3: *image* : tìm tất cả các folder con có tên chứa chữ image
        /// VD4: *       : tìm tất cả các folder con
        /// </param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách folder con tìm được bên trong folder tại url</returns>
        public static DirectoryInfo[] SearchFolders(string url, string searchPattern, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về false
            if (!IsExists(url))
            {
                return null;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            //3. Trả về danh sách thông tin các folder
            return folder.GetDirectories(searchPattern, searchOption);
        }

        /// <summary>
        /// Tìm kiếm danh sách file bên trong folder tại url.
        /// Trả về danh sách file tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm file bên trong.</param>
        /// <param name="searchPattern">
        /// Mẫu để tìm kiếm. 
        /// VD1:  image* : tìm tất cả các file có tên bắt đầu là image. 
        /// VD2: *image  : tìm tất cả các file có tên kết thúc là image
        /// VD3: *image* : tìm tất cả các file có tên chứa chữ image
        /// VD4: *.jpg   : tìm tất cả các file có phần mở rộng là .jpg
        /// VD5: image.* : tìm tất cả các file có tên là image với phần mở rộng là bất kỳ
        /// VD6: *       : tìm tất cả các file
        /// </param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách file tìm được bên trong folder tại url</returns>
        public static FileInfo[] SearchFiles(string url, string searchPattern, SearchOption searchOption)
        {
            //1. Kiểm tra nếu folder tại url không tồn tại thì trả về false
            if (!IsExists(url))
            {
                return null;
            }

            //2. Load folder từ url
            DirectoryInfo folder = new DirectoryInfo(url);

            //3. Trả về danh sách thông tin các file
            return folder.GetFiles(searchPattern, searchOption);
        }

        /// <summary>
        /// Tìm kiếm danh sách tên folder con bên trong folder tại url.
        /// Trả về danh sách tên folder con tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm folder con bên trong.</param>
        /// <param name="searchPattern">
        /// Mẫu để tìm kiếm. 
        /// VD1:  image* : tìm tất cả các folder con có tên bắt đầu là image. 
        /// VD2: *image  : tìm tất cả các folder con có tên kết thúc là image
        /// VD3: *image* : tìm tất cả các folder con có tên chứa chữ image
        /// VD4: *       : tìm tất cả các folder con
        /// </param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách tên folder con tìm được bên trong folder tại url</returns>
        public static string[] SearchFolderUrls(string url, string searchPattern, SearchOption searchOption)
        {
            //1. Load danh sách các folder info có trong folder tại url
            DirectoryInfo[] folderInfo = SearchFolders(url, searchPattern, searchOption);

            //2. Nếu không có folder con nào trong folder thì trả về null
            if (folderInfo == null)
                return null;

            //3. Trích lọc folder url từ các folder info
            var folderUrlList = from item in folderInfo
                                select item.FullName;

            //4. Trả kết quả về dạng mảng
            return folderUrlList.ToArray();
        }

        /// <summary>
        /// Tìm kiếm danh sách tên file bên trong folder tại url.
        /// Trả về danh sách tên file tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm file bên trong.</param>
        /// <param name="searchPattern">
        /// Mẫu để tìm kiếm. 
        /// VD1:  image* : tìm tất cả các file có tên bắt đầu là image. 
        /// VD2: *image  : tìm tất cả các file có tên kết thúc là image
        /// VD3: *image* : tìm tất cả các file có tên chứa chữ image
        /// VD4: *.jpg   : tìm tất cả các file có phần mở rộng là .jpg
        /// VD5: image.* : tìm tất cả các file có tên là image với phần mở rộng là bất kỳ
        /// VD6: *       : tìm tất cả các file
        /// </param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách tên file tìm được bên trong folder tại url</returns>
        public static string[] SearchFileUrls(string url, string searchPattern, SearchOption searchOption)
        {
            //1. Load danh sách các file info có trong folder tại url
            FileInfo[] fileInfo = SearchFiles(url, searchPattern, searchOption);

            //2. Nếu không có file nào trong folder thì trả về null
            if (fileInfo == null)
                return null;

            //3. Trích lọc file url từ các file info
            var fileUrlList = from item in fileInfo
                              select item.FullName;

            //4. Trả kết quả về dạng mảng
            return fileUrlList.ToArray();
        }

        /// <summary>
        /// Liệt kê danh sách folder con bên trong folder tại url.
        /// Trả về danh sách folder con bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần liệt kê folder con bên trong.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách folder con bên trong folder tại url</returns>
        public static DirectoryInfo[] ListFolderInfos(string url, SearchOption searchOption)
        {
            return SearchFolders(url, "*", searchOption);
        }

        /// <summary>
        /// Liệt kê danh sách file bên trong folder tại url.
        /// Trả về danh sách file bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần liệt kê file bên trong.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi file bên trong.</param>
        /// <returns>Trả về danh sách file bên trong folder tại url</returns>
        public static FileInfo[] ListFileInfos(string url, SearchOption searchOption)
        {
            return SearchFiles(url, "*", searchOption);
        }

        /// <summary>
        /// Liệt kê danh sách tên folder con bên trong folder tại url.
        /// Trả về danh sách tên folder con tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm folder con bên trong.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi folder con bên trong.</param>
        /// <returns>Trả về danh sách tên folder con bên trong folder tại url</returns>
        public static string[] ListFolderUrls(string url, SearchOption searchOption)
        {
            return SearchFolderUrls(url, "*", searchOption);
        }

        /// <summary>
        /// Liệt kê danh sách tên file bên trong folder tại url.
        /// Trả về danh sách tên file tìm được bên trong folder tại url
        /// </summary>
        /// <param name="url">Đường dẫn đến folder cần tìm kiếm file bên trong.</param>
        /// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong folder hiện tại; AllDirectories: Bao gồm folder hiện tại và mọi file bên trong.</param>
        /// <returns>Trả về danh sách tên file bên trong folder tại url</returns>
        public static string[] ListFileUrls(string url, SearchOption searchOption)
        {
            return SearchFileUrls(url, "*", searchOption);
        }

        /// <summary>
        /// Liệt kê danh sách tên các ổ đĩa trong máy tính.
        /// Trả về danh sách tên các ổ đĩa trong máy tính.
        /// </summary>
        /// <returns>Trả về danh sách tên các ổ đĩa trong máy tính.</returns>
        public static DriveInfo[] GetAllDrivers()
        {
            return DriveInfo.GetDrives();
        }

        /// <summary>
        /// Liệt kê danh sách tên các ổ đĩa trong máy tính.
        /// Trả về danh sách tên các ổ đĩa trong máy tính.
        /// </summary>
        /// <returns>Trả về danh sách tên các ổ đĩa trong máy tính.</returns>
        public static string[] GetAllDriverUrls()
        {
            //1. Load danh sách các driver info có trong máy tính
            DriveInfo[] driverInfo = GetAllDrivers();

            //2. Nếu không có driver nào thì trả về null
            if (driverInfo == null)
                return null;

            //3. Trích lọc driver url từ các folder info
            var driverUrlList = from item in driverInfo
                                select item.Name;

            //4. Trả kết quả về dạng mảng
            return driverUrlList.ToArray();
        }
    }
}
