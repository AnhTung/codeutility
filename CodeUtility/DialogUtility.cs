﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeUtility
{
    public class DialogUtility
    {
        #region FolderDialog
        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn thư mục
        /// </summary>
        /// <returns>
        /// Trả về đường dẫn đến thư mục đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn thư mục, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FolderDialog()
        {
            //1. Tạo hộp thoại Chọn Thư Mục
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            //2. Hiện hộp thoại Chọn Thư Mục
            DialogResult result = dialog.ShowDialog();

            //3. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.SelectedPath;

            //4. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn thư mục
        /// </summary>
        /// <param name="showNewFolderButton">
        /// Thiết lập Ẩn/Hiện nút thêm thư mục mới. True: hiện; False: ẩn
        /// </param>
        /// <returns>
        /// Trả về đường dẫn đến thư mục đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn thư mục, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FolderDialog(bool showNewFolderButton)
        {
            //1. Tạo hộp thoại Chọn Thư Mục
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            //2. Thiết lập Ẩn/Hiện nút thêm thư mục mới
            dialog.ShowNewFolderButton = showNewFolderButton;

            //3. Hiện hộp thoại Chọn Thư Mục
            DialogResult result = dialog.ShowDialog();

            //4. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.SelectedPath;

            //5. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn thư mục
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <param name="showNewFolderButton">
        /// Thiết lập Ẩn/Hiện nút thêm thư mục mới. True: hiện; False: ẩn
        /// </param>
        /// <returns>
        /// Trả về đường dẫn đến thư mục đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn thư mục, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FolderDialog(string title, bool showNewFolderButton)
        {
            //1. Tạo hộp thoại Chọn Thư Mục
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            //2. Thiết lập tiêu đề hộp thoại
            dialog.Description = title;

            //2. Thiết lập Ẩn/Hiện nút thêm thư mục mới
            dialog.ShowNewFolderButton = showNewFolderButton;

            //3. Hiện hộp thoại Chọn Thư Mục
            DialogResult result = dialog.ShowDialog();

            //4. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.SelectedPath;

            //5. Ngược lại, trả về rỗng
            return string.Empty;
        }
        #endregion

        #region FileDialog
        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn file
        /// </summary>
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileDialog()
        {
            //1. Tạo hộp thoại Chọn File
            OpenFileDialog dialog = new OpenFileDialog();

            //2. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //3. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //4. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn file
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileDialog(string title)
        {
            //1. Tạo hộp thoại Chọn File
            OpenFileDialog dialog = new OpenFileDialog();

            //2. Thiết lập tiêu đề hộp thoại
            dialog.Title = title;

            //3. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //4. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //5. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép chọn file
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <param name="filter">
        /// Thiết lập bộ lọc loại file
        /// VD: "Text file|*.txt|Image file|*.jpg;*.jpeg;*.gif;*.png|All file|*.*"
        /// </param>
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileDialog(string title, string filter)
        {
            //1. Tạo hộp thoại Chọn File
            OpenFileDialog dialog = new OpenFileDialog();

            //2. Thiết lập tiêu đề hộp thoại
            dialog.Title = title;

            //3. Thiết lập bộ lọc loại file
            dialog.Filter = filter;

            //4. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //5. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //6. Ngược lại, trả về rỗng
            return string.Empty;
        }
        #endregion

        #region FileSaveDialog
        /// <summary>
        /// Hiển thị một hộp thoại cho phép lưu file
        /// </summary>
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn, hoặc tên file mới nhập
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileSaveDialog()
        {
            //1. Tạo hộp thoại Chọn SaveFile
            SaveFileDialog dialog = new SaveFileDialog();

            //2. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //3. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //4. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép lưu file
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn, hoặc tên file mới nhập
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileSaveDialog(string title)
        {
            //1. Tạo hộp thoại Chọn SaveFile
            SaveFileDialog dialog = new SaveFileDialog();

            //2. Thiết lập tiêu đề hộp thoại save file
            dialog.Title = title;

            //3. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //4. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //5. Ngược lại, trả về rỗng
            return string.Empty;
        }

        /// <summary>
        /// Hiển thị một hộp thoại cho phép lưu file
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// Thiết lập bộ lọc loại file sẽ lưu
        /// VD: "Text file|*.txt|Image file|*.jpg;*.jpeg;*.gif;*.png|All file|*.*"
        /// <returns>
        /// Trả về đường dẫn đến file đã chọn, hoặc tên file mới nhập
        /// </returns>
        /// <remarks>
        /// Nếu không chọn file, thì trả về chuỗi rỗng
        /// </remarks>
        public static string FileSaveDialog(string title, string filter)
        {
            //1. Tạo hộp thoại Chọn SaveFile
            SaveFileDialog dialog = new SaveFileDialog();

            //2. Thiết lập tiêu đề hộp thoại save file
            dialog.Title = title;

            //3. Thiết lập bộ lọc loại file để save
            dialog.Filter = filter;

            //4. Hiện hộp thoại Chọn File
            DialogResult result = dialog.ShowDialog();

            //5. Nếu người dùng có Chọn Thư Mục, thì trả về URL đã chọn
            if (result == DialogResult.OK)
                return dialog.FileName;

            //6. Ngược lại, trả về rỗng
            return string.Empty;
        }
        #endregion

        #region MessageBox
        /// <summary>
        /// Hiển thị một hộp thoại xác nhận, có 2 nút: Yes và No
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <param name="message">
        /// Nội dung thông báo
        /// </param>
        /// <returns>
        /// Nếu chọn Yes, thì trả về True; Ngược lại, thì trả về False
        /// </returns>
        public static bool Confirm(string title, string message)
        {
            DialogResult result;
            result = MessageBox.Show(message, title, MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Hiển thị một hộp thoại yêu cầu người sử dụng nhập một chuỗi theo yêu cầu.
        /// Trả về giá trị người sử dụng đã nhập. Nếu người dùng không nhập, trả về giá trị rỗng.
        /// </summary>
        /// <param name="title">
        /// Tiêu đề của hộp thoại
        /// </param>
        /// <param name="message">
        /// Nội dung thông báo
        /// </param>
        /// <param name="buttonText">Tiêu đề của nút xác nhận trong hộp thoại.</param>
        /// <param name="width">Chiều dài của hộp thoại. Đơn vị tính là px.</param>
        /// <param name="formPadding">Khoảng cách lề 4 cạnh của Form.</param>
        /// <returns>Trả về giá trị người sử dụng đã nhập. Nếu người dùng không nhập, trả về giá trị rỗng.</returns>
        public static string Promt(string title, string message, string buttonText, int width, int formPadding)
        {
            //Tạo khoảng cách giữa các đối tượng trên Form
            int controlMargin = 3;

            //Tạo một form mới
            Form miniForm = new Form();

            //Thiết lập chiều dài cho Form
            miniForm.Width = width;

            //Thiết lập thanh tiêu đề
            miniForm.Text = title;
            miniForm.MaximizeBox = false;
            miniForm.MinimizeBox = false;

            //Thiết lập vị trí hiển thị
            miniForm.StartPosition = FormStartPosition.CenterScreen;

            //Không cho Resize form
            miniForm.FormBorderStyle = FormBorderStyle.FixedDialog;

            //Tạo một Label để hiện thông điệp
            Label Label_Message = new Label();
            Label_Message.Left = formPadding;
            Label_Message.Top = formPadding;
            Label_Message.Width = width;
            Label_Message.Height = 15;
            Label_Message.Text = message;

            //Tạo một Textbox cho phép nhập nội dung vào bên trong
            TextBox TextBox_Text = new TextBox();
            TextBox_Text.Left = formPadding;
            TextBox_Text.Top = Label_Message.Top + Label_Message.Height + controlMargin;
            TextBox_Text.Width = miniForm.ClientSize.Width - formPadding * 2;
            TextBox_Text.Anchor = AnchorStyles.Left | AnchorStyles.Right;

            //Tạo một Button
            Button Button_OK = new Button();
            Button_OK.Width = 100;
            Button_OK.Text = buttonText;
            Button_OK.Top = TextBox_Text.Top + TextBox_Text.Height + controlMargin;
            Button_OK.Left = TextBox_Text.Left + TextBox_Text.Width + 1 - Button_OK.Width;
            Button_OK.Click += (sender, e) => { miniForm.Close(); };

            //Thiết lập lại chiều cao cho Form
            miniForm.Height = Button_OK.Top + Button_OK.Height + formPadding + 40;

            //Đưa các Control vào trang
            miniForm.Controls.Add(TextBox_Text);
            miniForm.Controls.Add(Button_OK);
            miniForm.Controls.Add(Label_Message);

            //Thiết lập nút mặc định khi nhấn Enter
            miniForm.AcceptButton = Button_OK;

            //Hiển thị hộp thoại lên
            miniForm.ShowDialog();

            //Khi nhấn OK thì trả về kết quả là giá trị đã nhập trong ô TextBox;
            return TextBox_Text.Text;
        }
        #endregion
    }
}
