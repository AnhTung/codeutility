﻿using System;
using System.IO;
namespace CodeUtility
{
    public class FileUtility
    {
        #region ReadFile
        /// <summary>
        /// Đọc nội dung file
        /// </summary>
        /// <param name="url">
        /// Đường dẫn đến file cần đọc
        /// </param>
        /// <returns>
        /// Trả về nội dung của file đọc được
        /// </returns>
        public static string ReadFile(string url)
        {
            //1. Khai báo biến chứa nội dung file
            string text = string.Empty;

            //2. Khai báo đối tượng đọc file
            StreamReader reader = new StreamReader(url);

            //3. Đọc nội dung file
            text = reader.ReadToEnd();

            //4. Thoát file
            reader.Close();

            //5. Trả về kết quả
            return text;
        }

        /// <summary>
        /// Đọc nội dung file
        /// </summary>
        /// <param name="url">
        /// Đường dẫn đến file cần đọc
        /// </param>
        /// <param name="error">
        /// Chứa lỗi ngoại lệ (nếu có) trong quá trình đọc file
        /// </param>
        /// <returns>
        /// Trả về nội dung của file đọc được
        /// </returns>
        /// <remarks>
        /// Nếu có lỗi ngoại lệ xảy ra, thì trả về String.Empty
        /// </remarks>
        public static string ReadFile(string url, ref Exception error)
        {
            //1. Khai báo biến chứa nội dung file
            string text = string.Empty;

            try
            {
                //2. Khai báo đối tượng đọc file
                StreamReader reader = new StreamReader(url);

                //3. Đọc nội dung file
                text = reader.ReadToEnd();

                //4. Thoát file
                reader.Close();
            }
            catch (Exception ex)
            {
                //Xuất lỗi bắt được (ex) ra ngoài hàm theo biến (error)
                error = ex;
                //Trả về rỗng
                return string.Empty;
            }

            //5. Trả về kết quả
            return text;
        }
        #endregion

        #region WriteFile
        /// <summary>
        /// Ghi nội dung vào file
        /// </summary>
        /// <param name="url">
        /// Đường dẫn đến vị trí file sẽ được ghi
        /// </param>
        /// <param name="text">
        /// Nội dung sẽ ghi vào file
        /// </param>
        /// <returns>
        /// Nếu ghi file thành công, trả về true; ngược lại, trả về false
        /// </returns>
        public static bool WriteFile(string url, string text)
        {
            //1. Khai báo biến chứa kết quả
            bool result = false;

            //2. Khai báo đối tượng ghi file
            StreamWriter writer = new StreamWriter(url);

            //3. Ghi file
            writer.Write(text);

            //4. Thoát file
            writer.Close();

            //5. Cho kết quả bằng true (đã ghi file)
            result = true;

            //6. Trả về kết quả
            return result;
        }

        /// <summary>
        /// Ghi nội dung vào file
        /// </summary>
        /// <param name="url">
        /// Đường dẫn đến vị trí file sẽ được ghi
        /// </param>
        /// <param name="text">
        /// Nội dung sẽ ghi vào file
        /// </param>
        /// <param name="error">
        /// Chứa lỗi ngoại lệ (nếu có) trong quá trình ghi file
        /// </param>
        /// <returns>
        /// Nếu ghi file thành công, trả về true; ngược lại, trả về false
        /// </returns>
        /// <remarks>
        /// Nếu có lỗi ngoại lệ xảy ra, thì trả về False
        /// </remarks>
        public static bool WriteFile(string url, string text, ref Exception error)
        {
            //1. Khai báo biến chứa kết quả
            bool result = false;

            try
            {
                //2. Khai báo đối tượng ghi file
                StreamWriter writer = new StreamWriter(url);

                //3. Ghi file
                writer.Write(text);

                //4. Thoát file
                writer.Close();
            }
            catch (Exception ex)
            {
                //Xuất lỗi bắt được (ex) ra ngoài hàm theo biến (error)
                error = ex;
                //Trả về false (chưa ghi file)
                return false;
            }

            //5. Cho kết quả bằng true (đã ghi file)
            result = true;

            //6. Trả về kết quả
            return result;
        }
		#endregion

		#region ManagementFile

		/// <summary>
		/// Kiểm tra sự tồn tại của một file tại url.
		/// Nếu file có tồn tại: trả về true; ngược lại trả về false.
		/// Nếu url là null, empty hoặc không hợp lệ, kết quả trả về là false.
		/// </summary>
		/// <param name="url">Đường dẫn đến file cần kiểm tra tồn tại.</param>
		/// <returns>Nếu file có tồn tại: trả về true; ngược lại trả về false.</returns>
		public static bool IsExists(string url)
		{
			return File.Exists(url);
		}

		/// <summary>
		/// Tính tổng dung lượng của một file tại url. Đơn vị tính là kilobyte.
		/// Nếu url không tồn tại, trả về 0.
		/// </summary>
		/// <param name="url">Đường dẫn đến file cần tính dung lượng.</param>
		/// <param name="searchOption">Có 2 tùy chọn: TopDirectoryOnly: chỉ xét trong file hiện tại; AllDirectories: Bao gồm file hiện tại và mọi file con bên trong.</param>
		/// <returns>Trả về tổng dung lượng của một file. Đơn vị tính là kilobyte.</returns>
		public static long GetSize(string url)
		{
			//1. Kiểm tra nếu file tại url không tồn tại thì trả về 0
			if (!IsExists(url))
				return 0;

			//2. Tạo một biến để chứa dung lượng
			long size = 0;

			//3. Load file từ url
			FileInfo file = new FileInfo(url);

			//4. Tính dung lượng của file
			size = file.Length;

			//5. Trả về kết quả
			return size;
		}

		/// <summary>
		/// Di chuyển một file từ url đến newUrl. Có bắt lỗi ngoại lệ (nếu có).
		/// Nếu di chuyển thành công: trả về true; ngược lại trả về false.
		/// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
		/// </summary>
		/// <param name="url">Đường dẫn đến file cần chuyển đi. Ví dụ: C:\Images\abc.txt</param>
		/// <param name="newUrl">Đường dẫn đến file cần chuyển đến. Ví dụ: C:\Document\Images\abc.txt</param>
		/// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình di chuyển file.</param>
		/// <returns>Nếu di chuyển thành công: trả về true; ngược lại trả về false.</returns>
		public static bool Move(string url, string newUrl, ref Exception error)
		{
			//1. Kiểm tra nếu file tại url không tồn tại thì trả về false
			if (!IsExists(url))
			{
				string message = string.Format("The file with path is not exist: {0}", url);
				error = new Exception(message);
				return false;
			}

			//2. Load file từ url
			FileInfo file = new FileInfo(url);

			try
			{
				//3. Di chuyển file sang newUrl
				file.MoveTo(newUrl);
			}
			catch (Exception ex)
			{
				error = ex;
				return false;
			}

			//4. Trả về kết quả thành công
			return true;
		}

		/// <summary>
		/// Đổi tên một file sang tên mới. Có bắt lỗi ngoại lệ (nếu có).
		/// Nếu đổi tên thành công: trả về true; ngược lại trả về false.
		/// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
		/// </summary>
		/// <param name="url">Đường dẫn đến file cần đổi tên. Ví dụ: C:\Images\abc.txt</param>
		/// <param name="newName">Tên mới của file. Ví dụ: bcd.txt</param>
		/// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình đổi tên file.</param>
		/// <returns>Nếu đổi tên thành công: trả về true; ngược lại trả về false.</returns>
		public static bool Rename(string url, string newName, ref Exception error)
		{
			//1. Kiểm tra nếu file tại url không tồn tại thì trả về false
			if (!IsExists(url))
			{
				string message = string.Format("The file with path is not exist: {0}", url);
				error = new Exception(message);
				return false;
			}

			//2. Load file từ url
			FileInfo file = new FileInfo(url);

			//3. Load folder cha của url
			DirectoryInfo parentFolder = file.Directory;

			//4. Tạo url với tên mới
			string newUrl = Path.Combine(parentFolder.FullName, newName);

			//5. Di chuyển file từ url sang newUrl
			return Move(url, newUrl, ref error);
		}

		/// <summary>
		/// Xóa một file. Có bắt lỗi ngoại lệ (nếu có).
		/// Nếu xóa thành công hoặc file không tồn tại: trả về true; ngược lại trả về false.
		/// Nếu có lỗi ngoại lệ xảy ra thì trả về false.
		/// </summary>
		/// <param name="url">Đường dẫn đến file cần xóa. Ví dụ: C:\Images\abc.txt</param>
		/// <param name="error">Chứa lỗi ngoại lệ (nếu có) trong quá trình xóa file.</param>
		/// <returns>Nếu xóa thành công hoặc file không tồn tại: trả về true; ngược lại trả về false.</returns>
		public static bool Delete(string url, ref Exception error)
		{
			//1. Kiểm tra nếu file tại url không tồn tại thì trả về true
			if (!IsExists(url))
			{
				return true;
			}

			//2. Load file từ url
			FileInfo file = new FileInfo(url);

			try
			{
				//3. Xóa file
				file.Delete();
			}
			catch (Exception ex)
			{
				error = ex;
				return false;
			}

			//4. Trả về kết quả thành công
			return true;
		}

		#endregion

	}
}