﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CodeUtility
{
    public enum FitSizeType
    {
        Width = 1,
        Height = 2
    }

    public enum WaterMaskPositionType
    {
        MiddleCenter = 0,
        TopLeft = 1,
        TopRight = 2,
        BottomLeft = 3,
        BottomRight = 4
    }

	public enum StringCaseType
	{
		None = 0,
		UpperCase = 1,
		LowerCase = 2,
		TitleCase = 3
	}
}
