﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeUtility
{
	public static class StringUtility
	{
		public static string RemoveDoubleSpace(this string value)
		{
			value = Regex.Replace(value, @"\s+", " ");
			return value;
		}

		public static string ToUpperCase(this string value, bool removeDoubleSpace = true)
		{
			value = value.ToStringSafe();
			value = value.ToUpper();

			if (removeDoubleSpace)
				value = value.RemoveDoubleSpace();
			return value;
		}

		public static string ToLowerCase(this string value, bool removeDoubleSpace = true)
		{
			value = value.ToStringSafe();
			value = value.ToLower();

			if (removeDoubleSpace)
				value = value.RemoveDoubleSpace();
			return value;
		}

		public static string ToTitleCase(this string value, bool removeDoubleSpace = true)
		{
			value = value.ToLowerCase(removeDoubleSpace);

			TextInfo textInfo = CultureInfo.CurrentCulture.TextInfo;
			value = textInfo.ToTitleCase(value);
			return value;
        }

		public static string ToNoSings(this string value, bool removeDoubleSpace = true)
		{
			Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
			value = value.ToStringSafe();
			value = value.Normalize(NormalizationForm.FormD);
			value = regex.Replace(value, String.Empty);
			value = value.Replace('\u0111', 'd').Replace('\u0110', 'D');
			if (removeDoubleSpace)
				value = value.RemoveDoubleSpace();
			return value;
		}

		public static string ToUrlFormat(this string value, bool removeSign = true, StringCaseType stringCaseType = StringCaseType.LowerCase)
		{
			//1. Chuyển chuỗi về dạng chuỗi không Null
			value = value.ToStringSafe();
			//2. Gỡ toàn bộ dấu nếu cần
			if (removeSign)
				value = value.ToNoSings(true);
			//3. Định nghĩa một danh sách các ký tự đặc biệt
			string specialChar = "@#$%^&()+-*/\\={}[]|:;'\"`“”<>.,?!_~";
			//4. Loại bỏ các ký tự đặc biệt
			foreach (char item in specialChar.ToCharArray())
			{
				value = value.Replace(item, ' ');
			}
			//5. Loại bỏ khoảng trắng kép
			value = value.RemoveDoubleSpace();
			//6. Chuyển đổi kiểu chữ hoa - thường
			switch (stringCaseType)
			{
				case StringCaseType.LowerCase:
					value = value.ToLowerCase(true);
					break;
				case StringCaseType.UpperCase:
					value = value.ToUpperCase(true);
					break;
				case StringCaseType.TitleCase:
					value = value.ToTitleCase(true);
					break;
				case StringCaseType.None:
				default:
					break;
			}
			//7. Thay khoảng trắng bằng dấu gạch ngang (-)
			value = value.Replace(' ', '-');
			//8. Loại bỏ dấu gạch ngang ở 2 đầu văn bản
			value = value.Trim('-');
			//9. Trả về kết quả
			return value;
		}

        /// <summary>
        /// Split string with priority is left or right
        /// </summary>
        /// <param name="value"></param>
        /// <param name="removeSign">True:Is remove sign of string value</param>
        /// <param name="stringCaseType">Convert type string is Lower, Upper or Normal</param>
        /// <param name="removeDoubleSpace">True: Is remove double space</param>
        /// <returns>Return string</returns>
        public static string SubStringWithLength(this string value,int length,bool removeSign = false, StringCaseType stringCaseType = StringCaseType.None, bool removeDoubleSpace = false)
        {
            //1. Chuyển chuỗi về dạng chuỗi không Null
            value = value.ToStringSafe();
            //2. Gỡ toàn bộ dấu nếu cần
            if (removeSign)
                value = value.ToNoSings(true);
            //5. Loại bỏ khoảng trắng kép nếu cần
            if (removeDoubleSpace)
                value = value.RemoveDoubleSpace();
            //6. Chuyển đổi kiểu chữ hoa - thường
            switch (stringCaseType)
            {
                case StringCaseType.LowerCase:
                    value = value.ToLowerCase(true);
                    break;
                case StringCaseType.UpperCase:
                    value = value.ToUpperCase(true);
                    break;
                case StringCaseType.TitleCase:
                    value = value.ToTitleCase(true);
                    break;
                case StringCaseType.None:
                default:
                    break;
            }
            //7. Cắt chuỗi theo độ ưu tiên true:Left | false:Right
            if (value.Length > length)
            {
                int endIndex = value.IndexOf(" ", length);
                if (endIndex - length > 5)
                {
                    endIndex = value.LastIndexOf(" ", 0, length);
                }
                value = value.Substring(0,endIndex);
                value += "...";
            }
            return value;

        }
	}
}
