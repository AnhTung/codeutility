﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace EasyCode.Utility
{
    public static class SerializeUtility
    {
        #region Serialize
        public static string Serialize(object obj, Type type)
        {
            if (obj == null || type == null)
                return null;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);

            var ser = new XmlSerializer(type);
            ser.Serialize(writer, obj);

            return writer.ToString();
        }

        public static string Serialize<T>(T obj)
        {
            Type type = typeof(T);
            return Serialize(obj, type);
        }

        public static string Serialize(object obj, string typeAssemblyQualifiedName)
        {
            Type type = Type.GetType(typeAssemblyQualifiedName);
            return Serialize(obj, type);
        }
        #endregion

        #region SerializeList
        public static List<string> SerializeList(List<object> listObjs, Type type)
        {
            if (listObjs == null || listObjs.Count == 0 || type == null)
                return null;

            List<string> items = new List<string>();
            foreach (object item in listObjs)
            {
                items.Add(Serialize(item, type));
            }

            return items;
        }

        public static List<string> SerializeList<T>(List<object> listObjs)
        {
            Type type = typeof(T);
            return SerializeList(listObjs, type);
        }

        public static List<string> SerializeList(List<object> listObjs, string typeAssemblyQualifiedName)
        {
            Type type = Type.GetType(typeAssemblyQualifiedName);
            return SerializeList(listObjs, type);
        }
        #endregion

        #region Deserialize
        public static object Deserialize(string xml, Type type)
        {
            if (!Validator.IsString(xml) || type == null)
                return null;

            object result;

            var ser = new XmlSerializer(type);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);

            result = ser.Deserialize(reader);

            return result;
        }

        public static T Deserialize<T>(string xml)
        {
            Type type = typeof(T);
            return (T)Deserialize(xml, type);
        }

        public static object Deserialize(string xml, string typeAssemblyQualifiedName)
        {
            Type type = Type.GetType(typeAssemblyQualifiedName);
            return Deserialize(xml, type);
        }
        #endregion

        #region DeserializeList
        public static List<object> DeserializeList(string[] listXml, Type type)
        {
            if (listXml == null || listXml.Length == 0 || type == null)
                return null;

            List<object> listObj = new List<object>();
            foreach (string xml in listXml)
            {
                object item = SerializeUtility.Deserialize(xml, type);
                listObj.Add(item);
            }

            return listObj;
        }

        public static List<T> DeserializeList<T>(string[] listXml)
        {
            Type type = typeof(T);
            return DeserializeList(listXml, type) as List<T>;
        }

        public static List<object> DeserializeList(string[] listXml, string typeAssemblyQualifiedName)
        {
            Type type = Type.GetType(typeAssemblyQualifiedName);
            return DeserializeList(listXml, type);
        }
        #endregion
    } 
}