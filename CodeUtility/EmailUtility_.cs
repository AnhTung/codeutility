﻿using System.Net;
using System.Net.Mail;

namespace CodeUtility
{
    public class EmailUtility_
    {
        public static bool Send(string from, string password, string host, int port, bool enableSSL, string to, string subject, string body, bool isBodyHtml)
        {
            //1. Tạo các đối tượng hỗ trợ gửi email
            //1.1 Đối tượng chứa mail
            MailMessage mail = new MailMessage();
            //1.2 Đối tượng gửi  mail                
            SmtpClient smtp = new SmtpClient();
            //1.3 Tài khoản gửi  mail                    
            NetworkCredential credential = new NetworkCredential(); 

            //2. Thiết lập thông tin cho email
            //2.1 Gán địa chỉ gửi
            mail.From = new MailAddress(from);
            //2.2 Gán địa chỉ nhận
            mail.To.Add(to);
            //2.3 Gán chủ đề              
            mail.Subject = subject;
            //2.4 Gán nội dung          
            mail.Body = body;
            //2.5 Có chứa mã HTML?      
            mail.IsBodyHtml = isBodyHtml;      

            //3. Thiết lập tài khoản dùng để gửi email
            //3.1 Thiết lập email đăng nhập
            credential.UserName = from;
            //3.2 Thiết lập mật khẩu
            credential.Password = password;

            //4. Cấu hình thông số gửi email
            //4.1 Gán địa chỉ máy chủ
            smtp.Host = host;
            //4.2 Gán Port máy chủ             
            smtp.Port = port;
            //4.3 Có bảo mật SSL?          
            smtp.EnableSsl = enableSSL;
            //4.4 Tài khoản gửi mail   
            smtp.Credentials = credential;     

            //5. Gửi email
            smtp.Send(mail);

            return true;
        }
    }
}
