﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeUtility;

namespace Demo_ConvertData
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        private void button_Cong_Click(object sender, EventArgs e)
        {
            int? giaTri1 = textBox_SoThuNhat.Text.ToInt(null);
            int? giaTri2 = textBox_SoThuHai.Text.ToInt(null);

            if(giaTri1 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuNhat.Focus();
                return;
            }

            if (giaTri2 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuHai.Focus();
                return;
            }

            int ketQua = giaTri1.Value + giaTri2.Value;

            //5. Hiện lên màn hình
            string phepTinh = string.Format("{0} + {1} = {2}", giaTri1, giaTri2, ketQua);
            textBox_KetQua.Text = phepTinh;
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            textBox_SoThuNhat.Text = string.Empty;
            textBox_SoThuHai.Text = string.Empty;
            textBox_KetQua.Text = string.Empty;
        }

        private void button_Tru_Click(object sender, EventArgs e)
        {
            long? giaTri1 = textBox_SoThuNhat.Text.ToLong(null);
            long? giaTri2 = textBox_SoThuHai.Text.ToLong(null);

            if (giaTri1 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuNhat.Focus();
                return;
            }

            if (giaTri2 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuHai.Focus();
                return;
            }

            long ketQua = giaTri1.Value - giaTri2.Value;

            //5. Hiện lên màn hình
            string phepTinh = string.Format("{0} - {1} = {2}", giaTri1, giaTri2, ketQua);
            textBox_KetQua.Text = phepTinh;
        }

        private void button_Nhan_Click(object sender, EventArgs e)
        {
            float? giaTri1 = textBox_SoThuNhat.Text.ToFloat(null);
            float? giaTri2 = textBox_SoThuHai.Text.ToFloat(null);

            if (giaTri1 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuNhat.Focus();
                return;
            }

            if (giaTri2 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuHai.Focus();
                return;
            }

            float ketQua = giaTri1.Value * giaTri2.Value;

            //5. Hiện lên màn hình
            string phepTinh = string.Format("{0} x {1} = {2}", giaTri1, giaTri2, ketQua);
            textBox_KetQua.Text = phepTinh;
        }

        private void button_Chia_Click(object sender, EventArgs e)
        {
            decimal? giaTri1 = textBox_SoThuNhat.Text.ToDecimal(null);
            decimal? giaTri2 = textBox_SoThuHai.Text.ToDecimal(null);

            if (giaTri1 == null)
            {
                MessageBox.Show("Vui lòng nhập số nguyên");
                textBox_SoThuNhat.Focus();
                return;
            }

            if (giaTri2 == null || giaTri2 == 0)
            {
                MessageBox.Show("Vui lòng nhập số nguyên khác 0");
                textBox_SoThuHai.Focus();
                return;
            }

            decimal ketQua = giaTri1.Value / giaTri2.Value;
            ketQua = Math.Round(ketQua, 2);

            //5. Hiện lên màn hình
            string phepTinh = string.Format("{0} / {1} = {2}", giaTri1, giaTri2, ketQua);
            textBox_KetQua.Text = phepTinh;
        }

        private void button_Convert_Click(object sender, EventArgs e)
        {
            string dateInput  = textBox_DateInput.Text.Trim();
            string dateFormat = textBox_DateFormat.Text.Trim();

            DateTime? date = dateInput.ToDateTime(null, dateFormat);

            if (date == null)
                textBox_DateOutput.Text = "Ngày không hợp lệ";
            else
                textBox_DateOutput.Text = date.Value.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
