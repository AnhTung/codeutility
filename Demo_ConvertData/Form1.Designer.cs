﻿namespace Demo_ConvertData
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_SoThuNhat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_SoThuHai = new System.Windows.Forms.TextBox();
            this.button_Cong = new System.Windows.Forms.Button();
            this.button_Tru = new System.Windows.Forms.Button();
            this.button_Nhan = new System.Windows.Forms.Button();
            this.button_Chia = new System.Windows.Forms.Button();
            this.textBox_KetQua = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_DateInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_DateFormat = new System.Windows.Forms.TextBox();
            this.button_Convert = new System.Windows.Forms.Button();
            this.textBox_DateOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số thứ 1:";
            // 
            // textBox_SoThuNhat
            // 
            this.textBox_SoThuNhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_SoThuNhat.Location = new System.Drawing.Point(86, 10);
            this.textBox_SoThuNhat.Name = "textBox_SoThuNhat";
            this.textBox_SoThuNhat.Size = new System.Drawing.Size(83, 23);
            this.textBox_SoThuNhat.TabIndex = 0;
            this.textBox_SoThuNhat.Text = "1";
            this.textBox_SoThuNhat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số thứ 2:";
            // 
            // textBox_SoThuHai
            // 
            this.textBox_SoThuHai.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_SoThuHai.Location = new System.Drawing.Point(86, 36);
            this.textBox_SoThuHai.Name = "textBox_SoThuHai";
            this.textBox_SoThuHai.Size = new System.Drawing.Size(83, 23);
            this.textBox_SoThuHai.TabIndex = 1;
            this.textBox_SoThuHai.Text = "2";
            this.textBox_SoThuHai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_Cong
            // 
            this.button_Cong.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Cong.ForeColor = System.Drawing.Color.Blue;
            this.button_Cong.Location = new System.Drawing.Point(179, 10);
            this.button_Cong.Name = "button_Cong";
            this.button_Cong.Size = new System.Drawing.Size(46, 46);
            this.button_Cong.TabIndex = 2;
            this.button_Cong.Text = "+";
            this.button_Cong.UseVisualStyleBackColor = true;
            this.button_Cong.Click += new System.EventHandler(this.button_Cong_Click);
            // 
            // button_Tru
            // 
            this.button_Tru.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Tru.ForeColor = System.Drawing.Color.Blue;
            this.button_Tru.Location = new System.Drawing.Point(233, 10);
            this.button_Tru.Name = "button_Tru";
            this.button_Tru.Size = new System.Drawing.Size(46, 46);
            this.button_Tru.TabIndex = 3;
            this.button_Tru.Text = "-";
            this.button_Tru.UseVisualStyleBackColor = true;
            this.button_Tru.Click += new System.EventHandler(this.button_Tru_Click);
            // 
            // button_Nhan
            // 
            this.button_Nhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Nhan.ForeColor = System.Drawing.Color.Blue;
            this.button_Nhan.Location = new System.Drawing.Point(286, 10);
            this.button_Nhan.Name = "button_Nhan";
            this.button_Nhan.Size = new System.Drawing.Size(46, 46);
            this.button_Nhan.TabIndex = 4;
            this.button_Nhan.Text = "x";
            this.button_Nhan.UseVisualStyleBackColor = true;
            this.button_Nhan.Click += new System.EventHandler(this.button_Nhan_Click);
            // 
            // button_Chia
            // 
            this.button_Chia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Chia.ForeColor = System.Drawing.Color.Blue;
            this.button_Chia.Location = new System.Drawing.Point(340, 10);
            this.button_Chia.Name = "button_Chia";
            this.button_Chia.Size = new System.Drawing.Size(46, 46);
            this.button_Chia.TabIndex = 5;
            this.button_Chia.Text = "/";
            this.button_Chia.UseVisualStyleBackColor = true;
            this.button_Chia.Click += new System.EventHandler(this.button_Chia_Click);
            // 
            // textBox_KetQua
            // 
            this.textBox_KetQua.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_KetQua.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox_KetQua.Location = new System.Drawing.Point(396, 10);
            this.textBox_KetQua.Multiline = true;
            this.textBox_KetQua.Name = "textBox_KetQua";
            this.textBox_KetQua.Size = new System.Drawing.Size(296, 46);
            this.textBox_KetQua.TabIndex = 6;
            this.textBox_KetQua.Text = "1 + 2 = 3";
            this.textBox_KetQua.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày:";
            // 
            // textBox_DateInput
            // 
            this.textBox_DateInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DateInput.Location = new System.Drawing.Point(86, 70);
            this.textBox_DateInput.Name = "textBox_DateInput";
            this.textBox_DateInput.Size = new System.Drawing.Size(193, 23);
            this.textBox_DateInput.TabIndex = 7;
            this.textBox_DateInput.Text = "11/12/2014";
            this.textBox_DateInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Định dạng:";
            // 
            // textBox_DateFormat
            // 
            this.textBox_DateFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DateFormat.Location = new System.Drawing.Point(86, 96);
            this.textBox_DateFormat.Name = "textBox_DateFormat";
            this.textBox_DateFormat.Size = new System.Drawing.Size(193, 23);
            this.textBox_DateFormat.TabIndex = 8;
            this.textBox_DateFormat.Text = "dd/MM/yyyy";
            this.textBox_DateFormat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_Convert
            // 
            this.button_Convert.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Convert.ForeColor = System.Drawing.Color.Blue;
            this.button_Convert.Location = new System.Drawing.Point(286, 70);
            this.button_Convert.Name = "button_Convert";
            this.button_Convert.Size = new System.Drawing.Size(100, 49);
            this.button_Convert.TabIndex = 9;
            this.button_Convert.Text = "Convert";
            this.button_Convert.UseVisualStyleBackColor = true;
            this.button_Convert.Click += new System.EventHandler(this.button_Convert_Click);
            // 
            // textBox_DateOutput
            // 
            this.textBox_DateOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DateOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBox_DateOutput.Location = new System.Drawing.Point(396, 70);
            this.textBox_DateOutput.Multiline = true;
            this.textBox_DateOutput.Name = "textBox_DateOutput";
            this.textBox_DateOutput.Size = new System.Drawing.Size(296, 49);
            this.textBox_DateOutput.TabIndex = 10;
            this.textBox_DateOutput.Text = "11/12/2014 00:00:00";
            this.textBox_DateOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 126);
            this.Controls.Add(this.button_Chia);
            this.Controls.Add(this.button_Nhan);
            this.Controls.Add(this.button_Tru);
            this.Controls.Add(this.button_Convert);
            this.Controls.Add(this.button_Cong);
            this.Controls.Add(this.textBox_DateFormat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_SoThuHai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_DateInput);
            this.Controls.Add(this.textBox_DateOutput);
            this.Controls.Add(this.textBox_KetQua);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_SoThuNhat);
            this.Controls.Add(this.label1);
            this.Name = "Form_Main";
            this.Text = "Máy Tính";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_SoThuNhat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_SoThuHai;
        private System.Windows.Forms.Button button_Cong;
        private System.Windows.Forms.Button button_Tru;
        private System.Windows.Forms.Button button_Nhan;
        private System.Windows.Forms.Button button_Chia;
        private System.Windows.Forms.TextBox textBox_KetQua;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_DateInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_DateFormat;
        private System.Windows.Forms.Button button_Convert;
        private System.Windows.Forms.TextBox textBox_DateOutput;
    }
}

