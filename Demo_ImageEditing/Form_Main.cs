﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeUtility;

namespace Demo_ImageEditing
{
    public partial class Form_Main : Form
    {
        #region Fields
        //Tạo biến quyết định khả năng tạo vùng chọn
        bool isClicking;
        bool isDraping;
        int drapX;
        int drapY;
        //Tạo biến lưu trữ tọa độ khởi đầu của trục X
        int beginX;
        //Tạo biến lưu trữ tọa độ khởi đầu của trục Y
        int beginY;
        //Tạo biến lưu trữ vùng chọn
        Rectangle selectedArea;
        //Tạo biến lưu trữ tỉ lệ co giãn
        double displayRatio;
        //Tạo biến lưu trữ khoảng cách trên và dưới
        int xMargin;
        //Tạo biến lưu trữ khoảng cách trái và phải
        int yMargin;
        #endregion

        #region Helpers
        private void ShowImageInfo()
        {
            //1. Load hình từ PictureBox
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            //2. Kiểm tra nếu chưa chọn hình thì kết thúc
            if (sourceImage == null)
                return;

            //3. Tính kích cỡ thực sự của ảnh
            int imageWidth = sourceImage.Width;
            int imageHeight = sourceImage.Height;

            //4. Tính kích cỡ của khung ảnh trên Form
            int boxWidth = pictureBox_Main.Width;
            int boxHeight = pictureBox_Main.Height;

            //5. Nếu kích cỡ ảnh lớn hơn kích cỡ của khung ảnh, thì hiển thị dạng zoom
            if (imageWidth > boxWidth || imageHeight > boxHeight)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            //Ngược lại, hiển thị chỉnh giữa khung ảnh
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;

            //6. Tính toán tỉ lệ co giãn của ảnh khi hiển thị
            //6.1 Giả sử kích cỡ hiển thị là kích cỡ thật của ảnh
            int displayWidth = imageWidth;
            int displayHeight = imageHeight;

            //6.2 Nếu chiều dài của ảnh khi hiển thị > chiều dài của khung ảnh, 
            //thì tính tỉ lệ co giãn rồi bóp chiều dài của ảnh cho vừa với khung
            //Sau đó bóp chiều cao của ảnh theo tỉ lệ tương ứng.
            if (displayWidth > boxWidth)
            {
                displayRatio = displayWidth / boxWidth.ToDouble();
                displayWidth = boxWidth;
                displayHeight = Math.Round(displayHeight / displayRatio).ToInt();
            }

            //6.3 Tiếp tục kiểm tra, nếu chiều cao mới của ảnh khi hiển thị > chiều cao của khung ảnh, 
            //thì tính tỉ lệ co giãn rồi bóp chiều cao của ảnh cho vừa với khung
            //Sau đó bóp chiều dài của ảnh theo tỉ lệ tương ứng.
            if (displayHeight > boxHeight)
            {
                displayRatio = displayHeight / boxHeight.ToDouble();
                displayHeight = boxHeight;
                displayWidth = Math.Round(displayWidth / displayRatio).ToInt();
            }

            //6.4 Tính lại tỉ lệ co giãn cuối cùng, khi ảnh đã hiển thị vừa khung ảnh.
            displayRatio = displayWidth / imageWidth.ToDouble();

            //7. Tính khoảng cách lề trái-phải của ảnh khi hiển thị trong khung ảnh
            xMargin = Math.Round((boxWidth - displayWidth) / 2.ToDouble()).ToInt();

            //8. Tính khoảng cách lề trên-dưới của ảnh khi hiển thị trong khung ảnh
            yMargin = Math.Round((boxHeight - displayHeight) / 2.ToDouble()).ToInt();

            //9. Hiển thị kích thước thật của ảnh lên thanh trạng thái
            toolStripStatusLabel_WidthValue.Text = sourceImage.Width.ToString();
            toolStripStatusLabel_HeightValue.Text = sourceImage.Height.ToString();

            //10. Hiển thị kích thước đang hiển thị của ảnh lên thanh trạng thái
            toolStripStatusLabel_DisplayWidthValue.Text = displayWidth.ToString();
            toolStripStatusLabel_DisplayHeightValue.Text = displayHeight.ToString();

            //11. Hiển thị tỉ lệ co giãn lên thanh trạng thái
            toolStripStatusLabel_RatioValue.Text = Math.Round(displayRatio * 100, 2) + "%";

            //12. Hiển thị thông tin vùng chọn
            toolStripStatusLabel_CropXValue.Text = selectedArea.X.ToString();
            toolStripStatusLabel_CropYValue.Text = selectedArea.Y.ToString();
            toolStripStatusLabel_CropWidthValue.Text = selectedArea.Width.ToString();
            toolStripStatusLabel_CropHeightValue.Text = selectedArea.Height.ToString();
        }
        #endregion

        #region FormHandlers
        public Form_Main()
        {
            InitializeComponent();
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            pictureBox_Main.Padding = new Padding(0, 0, 0, 0);
            pictureBox_Main.Margin = new Padding(0, 0, 0, 0);
        }

        private void Form_Main_Resize(object sender, EventArgs e)
        {
            //1. Hiển thị thông tin lên thanh trạng thái
            ShowImageInfo();
        }
        #endregion

        #region MenuHandlers
        private void ToolStripMenuItem_Open_Click(object sender, EventArgs e)
        {
            //0. Reset vùng chọn (nếu có)
            selectedArea = new Rectangle(0, 0, 0, 0);

            //1. Hiển thị hộp thoại cho phép chọn ảnh
            string url = DialogUtility.FileDialog("Chọn hình để xem", "Image file|*.jpg;*.jpeg;*.gif;*.png");

            //2.Load ảnh từ URL đã chọn
            Bitmap sourceImage = ImageUtility.LoadImage(url);

            //3. Hiển thị ảnh lên khung ảnh
            pictureBox_Main.Image = sourceImage;

            //4. Thiết lập con trỏ chuột dạng mặc định
            pictureBox_Main.Cursor = Cursors.Default;

            //5. Hiển thị thông tin lên thanh trạng thái
            ShowImageInfo();
        }

        private void ToolStripMenuItem_Save_Click(object sender, EventArgs e)
        {
            //1. Hiển thị hộp thoại hỏi vị trí lưu (url)
            string url = DialogUtility.FileSaveDialog("Chọn vị trí để lưu ảnh", "Image file|*.jpg;*.jpeg;*.gif;*.png");

            //2. Kiểm tra nếu url rỗng thì kết thúc
            if (url == string.Empty)
                return;

            //3. Trích ảnh đang hiển thị trong PictureBox
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            //4. Khai báo resolution của ảnh
            int resolution = 72;

            //5. Khái báo chất lượng ảnh
            int quality = 90;

            //6. Lưu ảnh
            ImageUtility.SaveImage(sourceImage, resolution, quality, url);
        }

        private void ToolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            bool result = DialogUtility.Confirm("Nhắc nhở", "Bạn có muốn thoát chương trình không?");

            if (result)
                this.Close();
        }

        private void ToolStripMenuItem_Help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thực hiện bởi lớp LTV 2014", "Chương trình xử lý ảnh số");
        }
        #endregion

        #region ButtonsHanlers
        private void button_Resize_Click(object sender, EventArgs e)
        {
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;
            Bitmap newImage = ImageUtility.ResizeImage(sourceImage, 220, FitSizeType.Height);

            pictureBox_Main.Image = newImage;

            if (newImage.Width > pictureBox_Main.Width || newImage.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;
        }
        private void button_Resize_W_Click(object sender, EventArgs e)
        {
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;
            Bitmap newImage = ImageUtility.ResizeImage(sourceImage, 220, FitSizeType.Width);

            pictureBox_Main.Image = newImage;

            if (newImage.Width > pictureBox_Main.Width || newImage.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void button_WaterMask_Click(object sender, EventArgs e)
        {
            //1. Lấy hình đang mở trong PictureBox
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            //2. Kiểm tra nếu chưa mở hình thì kết thúc
            if (sourceImage == null)
            {
                MessageBox.Show("Vui lòng mở một hình để xử lý.");
                return;
            }

            //3. Bật 1 hộp thoại cho phép chọn hình làm WaterMask
            string url = DialogUtility.FileDialog("Chọn hình làm WaterMask", "Image file|*.jpg;*.jpeg;*.gif;*.png");

            //4. Kiểm tra nếu người dùng không chọn hình nào thì kết thúc
            if (url == string.Empty)
            {
                MessageBox.Show("Vui lòng chọn một hình làm WaterMask.");
                return;
            }

            //5. Load hình WaterMask từ url mà người dùng đã chọn
            Bitmap waterMaskImage = ImageUtility.LoadImage(url);

            //6. Thiết lập độ trong suốt
            int waterMaskOpacityPecent = 100;

            //7. Thiết lập tỉ lệ của waterMask so với hình gốc
            int waterMaskRatio = 100;

            //8. Thiết lập khoảng cách lề của waterMask so với biên của hình gốc
            int waterMaskMargin = 0;

            //9. Thiết lập vị trí đặt WaterMask
            WaterMaskPositionType waterMaskPositionType = WaterMaskPositionType.MiddleCenter;

            //10. Đóng dấu lên hình
            Bitmap result = ImageUtility.WaterMaskWithImage(sourceImage, waterMaskImage, waterMaskOpacityPecent, waterMaskRatio, waterMaskPositionType, waterMaskMargin);

            //11. Hiển thị hình mới lên PictureBox
            pictureBox_Main.Image = result;

            //12. Cấu hình kiểu hiển thị
            if (result.Width > pictureBox_Main.Width || result.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void button_TextMask_Click(object sender, EventArgs e)
        {
            //1. Lấy hình đang mở trong PictureBox
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            //2. Kiểm tra nếu chưa mở hình thì kết thúc
            if (sourceImage == null)
            {
                MessageBox.Show("Vui lòng mở một hình để xử lý.");
                return;
            }

            //3. Bật 1 hộp thoại cho phép nhập văn bản làm WaterMask
            string waterText = DialogUtility.Promt("WaterMask String", "Mời nhập 1 văn bản dùng làm Water Mask.", "OK", 500, 5);

            //4. Kiểm tra nếu người dùng không chọn hình nào thì kết thúc
            if (waterText == string.Empty)
            {
                MessageBox.Show("Bạn chưa nhập văn bản dùng làm Water Mask.");
                return;
            }

            //5. Thiết lập Font chữ
            string fontUrl = "../../Youngblood.TTF";

            //6. Thiết lập màu chữ
            Color textColor1 = Color.Yellow;
            Color textColor2 = Color.Red;

            //7. Thiết lập độ gradientAngle
            int gradientAngle = 90;

            //8. Thiết lập màu khung
            Color borderColor = Color.White;

            //9. Thiết lập cỡ khung
            int borderSize = 10;

            //10. Thiết lập màu nền
            Color backgroundColor = Color.Transparent;

            //11. Thiết lập kiểu chữ
            FontStyle fontStyle = FontStyle.Bold;

            //12. Thiết lập vị trí đặt WaterMask
            WaterMaskPositionType waterMaskPositionType = WaterMaskPositionType.TopRight;

            //13. Thiết lập độ trong suốt
            int waterMaskOpacityPecent = 100;

            //14. Thiết lập tỉ lệ của waterMask so với hình gốc
            int waterMaskRatio = 50;

            //15. Thiết lập khoảng cách lề của waterMask so với biên của hình gốc
            int waterMaskMargin = 20;

            //16. Đóng dấu lên hình
            Bitmap result = ImageUtility.WaterMaskWithString(sourceImage, waterText, fontUrl, waterMaskOpacityPecent, waterMaskRatio, waterMaskPositionType, waterMaskMargin, fontStyle, textColor1, textColor2, gradientAngle, backgroundColor, borderColor, borderSize);

            //17. Hiển thị hình mới lên PictureBox
            pictureBox_Main.Image = result;

            //18. Cấu hình kiểu hiển thị
            if (result.Width > pictureBox_Main.Width || result.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void button_LeftRotate_Click(object sender, EventArgs e)
        {
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            if (sourceImage == null)
                return;

            Bitmap newImage = ImageUtility.RotateImageToLeft(sourceImage);

            pictureBox_Main.Image = newImage;

            if (newImage.Width > pictureBox_Main.Width || newImage.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;

            //Hiển thị thông tin ảnh lên thanh trạng thái
            ShowImageInfo();
        }

        private void button_RightRotate_Click(object sender, EventArgs e)
        {
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            if (sourceImage == null)
                return;

            Bitmap newImage = ImageUtility.RotateImageToRight(sourceImage);

            pictureBox_Main.Image = newImage;

            if (newImage.Width > pictureBox_Main.Width || newImage.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;

            //Hiển thị thông tin ảnh lên thanh trạng thái
            ShowImageInfo();
        }

        private void button_Crop_Click(object sender, EventArgs e)
        {
            //1. Kiểm tra nếu chưa tạo vùng chọn thì kết thúc.
            if (selectedArea.Width == 0 || selectedArea.Height == 0)
                return;

            //2. Lấy hình đang hiển thị trong PictureBox
            Bitmap sourceImage = pictureBox_Main.Image as Bitmap;

            //3. Tính toán khu vực chọn với kích thức thật
            int X = Math.Round(selectedArea.X / displayRatio, 0).ToInt();
            int Y = Math.Round(selectedArea.Y / displayRatio, 0).ToInt();
            int Width = Math.Round((selectedArea.Width) / displayRatio, 0).ToInt();
            int Height = Math.Round((selectedArea.Height) / displayRatio, 0).ToInt();

            //4. Tạo vùng chọn với kích cỡ thật
            Rectangle realRectange = new Rectangle(X, Y, Width, Height);

            //5. Cắt cúp hình
            Bitmap newImage = ImageUtility.CropImage(sourceImage, realRectange);

            //6. Load hình vào Picturebox
            pictureBox_Main.Image = newImage;

            //7. Cập nhật lại hình nguồn
            sourceImage = newImage;

            //8. Nếu kích cỡ ảnh lớn hơn kích cỡ của khung ảnh, thì hiển thị dạng zoom
            if (sourceImage.Width > pictureBox_Main.Width || sourceImage.Height > pictureBox_Main.Height)
                pictureBox_Main.SizeMode = PictureBoxSizeMode.Zoom;
            //Ngược lại, hiển thị chỉnh giữa khung ảnh
            else
                pictureBox_Main.SizeMode = PictureBoxSizeMode.CenterImage;

            //9. Hủy vùng chọn
            selectedArea = new Rectangle(0, 0, 0, 0);

            //10. Hiển thị thông tin ảnh mới lên thanh trạng thái
            ShowImageInfo();
        }
        #endregion

        #region PictureBox
        private void pictureBox_Main_MouseDown(object sender, MouseEventArgs e)
        {
            //0. Nếu người dùng click chuột phải thì hủy vùng chọn rồi kết thúc
            if (e.Button == MouseButtons.Right)
            {
                //Hủy vùng chọn
                selectedArea = new Rectangle(0, 0, 0, 0);
                //Làm tươi hình
                pictureBox_Main.Refresh();
                //Đổi con trỏ chuột về dạng mặc định
                pictureBox_Main.Cursor = Cursors.Default;
                //Kết thúc
                return;
            }

            //1. Tính toán tọa độ của con trỏ chuột để kiểm tra xem người dùng đang click vào đâu
            int currentX = e.X - xMargin;
            int currentY = e.Y - yMargin;

            //2. Kiểm tra xem người dùng có đang click chuột trên vùng chọn đã tạo ra trước đó không?
            //Nếu đúng, chứng tỏ người dùng đang muốn di chuyển nó

            //2.1 Tính toán tọa độ X1, Y1 của vùng chọn
            int x1 = selectedArea.X;
            int y1 = selectedArea.Y;

            //2.2 Tính toán tọa độ X2, Y2 của vùng chọn
            int x2 = selectedArea.X + selectedArea.Width;
            int y2 = selectedArea.Y + selectedArea.Height;

            //2.3 Kiểm tra xem vị trí con trỏ chuột có đang nằm trong vùng chọn hay không?
            bool xInSelectedArea = false;
            bool yInSelectedArea = false;

            if (x1 <= currentX && currentX <= x2)
                xInSelectedArea = true;

            if (y1 <= currentY && currentY <= y2)
                yInSelectedArea = true;

            //2.4 Nếu đúng là con trỏ chuột đang được click bên trong vùng chọn
            if (xInSelectedArea && yInSelectedArea)
            {
                //Thiết lập trạng thái mới là đang di chuyển vùng chọn, chứ ko phải click bình thường
                isDraping = true;
                isClicking = false;

                //Cập nhật vị trí mới di chuyển đến là vị trí hiện tại của con trỏ chuột 
                drapX = currentX;
                drapY = currentY;

                //Thay con trỏ chuột sang kiểu đang di chuyển
                pictureBox_Main.Cursor = Cursors.NoMove2D;

                //Kết thúc
                return;
            }

            //3. Ngược lại, có nghĩa là người dùng đang click bên ngoài vùng chọn
            //Chứng tỏ người dùng muốn tạo một vùng chọn mới

            //3.1 Thiết lập trạng thái mới là đang click chuột chứ không phải kéo chuột
            isClicking = true;
            isDraping = false;

            //3.2 Đổi con trỏ chuột thành dạng chữ thập (kiểu tạo vùng chọn)
            pictureBox_Main.Cursor = Cursors.Cross;

            //3.3 Xem vị trí đang click chuột chính là điểm khởi đầu của vùng chọn mới
            beginX = currentX;
            beginY = currentY;
        }

        private void pictureBox_Main_MouseMove(object sender, MouseEventArgs e)
        {
            //1. Tính toán tọa độ hiện tại của con trỏ chuột
            int currentX = e.X - xMargin;
            int currentY = e.Y - yMargin;

            //2. Kiểm tra, nếu người dùng không click chuột và cũng không kéo chuột (chỉ rê chuột trên hình)
            //Thì hiển thị tọa độ hiện tại của con trỏ chuột lên thanh trạng thái rồi kết thúc
            if (!isClicking && !isDraping)
            {
                toolStripStatusLabel_XValue.Text = currentX.ToString();
                toolStripStatusLabel_YValue.Text = currentY.ToString();
                return;
            }

            //3. Ngược lại, kiểm tra xem người dùng có đang di chuyển vùng chọn không?
            //   Nếu có thì cập nhật lại vùng chọn tọa độ x1,x2 và y1,y2
            //   Lưu ý: khi di chuyển vùng chọn, chỉ có tọa độ thay đổi, kích thước vẫn giữa nguyên
            if (isDraping)
            {
                //3.1 Tính toán khoảng cách di chuyển theo X
                int changeX = currentX - drapX;
                //Tính toán khoảng cách di chuyển theo Y
                int changeY = currentY - drapY;
                //Ghi chú: drapX là vị trí của con trỏ chuột khi bắt đầu di chuyển, currentX là vị trí hiện tại của con trỏ chuột khi đang di chuyển

                //3.2 Lưu trữ tọa độ vùng chọn mới
                selectedArea = new Rectangle(selectedArea.X + changeX, selectedArea.Y + changeY, selectedArea.Width, selectedArea.Height);

                //3.3 Sau đó xem vị trí hiện tại của con trỏ chuột chính là vị trí bắt đầu cho bước di chuyển tiếp theo
                drapX = currentX;
                drapY = currentY;
            }
            //4. Ngược lại, chứng tỏ người dùng đang tạo vùng chọn mới
            else
            {
                //4.1 Tính toán tọa độ X1,Y1 của vùng chọn (điểm bắt đầu)
                int x1 = Math.Min(beginX, currentX);
                int y1 = Math.Min(beginY, currentY);

                //4.2 Tính toán tọa độ X2,Y2 của vùng chọn (điểm kết thúc)
                int x2 = Math.Max(beginX, currentX);
                int y2 = Math.Max(beginY, currentY);

                //4.3 Tính toán chiều dài của vùng chọn
                int width = x2 - x1;

                //4.4 Tính toán chiều cao của vùng chọn
                int height = y2 - y1;

                //4.5 Lưu trữ tọa độ vùng chọn
                selectedArea = new Rectangle(x1, y1, width, height);
            }

            //5. Làm tươi ảnh để cập nhật lại đường chọn trên hình
            pictureBox_Main.Refresh();

            //6. Hiển thị lên thanh trạng thái
            ShowImageInfo();

            //7. Kết thúc
            return;
        }

        private void pictureBox_Main_Paint(object sender, PaintEventArgs e)
        {
            //1. Kiểm tra vùng chọn, nếu chưa chọn vùng chọn thì kết thúc
            if (selectedArea.Width == 0 || selectedArea.Height == 0)
                return;

            //2. Tạo một đối tượng bút vẽ: mực đỏ, đầu bút = 1px
            Pen pen = new Pen(Color.Red, 1);

            //3. Tính toán khu vực chọn trên ảnh có cộng thêm margin
            Rectangle displayArea = new Rectangle();
            displayArea.X = selectedArea.X + xMargin;
            displayArea.Y = selectedArea.Y + yMargin;
            displayArea.Width = selectedArea.Width;
            displayArea.Height = selectedArea.Height;

            //4. Sử dụng bút vẽ, kẻ 1 đường quanh đường chọn
            e.Graphics.DrawRectangle(pen, displayArea);
        }

        private void pictureBox_Main_MouseUp(object sender, MouseEventArgs e)
        {
            //1. Phục hồi con trỏ chuột về dạng mặc định
            pictureBox_Main.Cursor = Cursors.Default;
            //2. Hủy trạng thái đang click giữ chuột trái
            isClicking = false;
            //3. Hủy trạng thái đang di chuyển vùng chọn
            isDraping = false;
        }

        private void pictureBox_Main_DoubleClick(object sender, EventArgs e)
        {
            ToolStripMenuItem_Open_Click(null, null);
        }
        #endregion
    }
}
