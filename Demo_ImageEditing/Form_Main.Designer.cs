﻿namespace Demo_ImageEditing
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip_Main = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_Main = new System.Windows.Forms.PictureBox();
            this.button_Resize = new System.Windows.Forms.Button();
            this.button_Crop = new System.Windows.Forms.Button();
            this.button_LeftRotate = new System.Windows.Forms.Button();
            this.button_RightRotate = new System.Windows.Forms.Button();
            this.button_WaterMask = new System.Windows.Forms.Button();
            this.button_TestMask = new System.Windows.Forms.Button();
            this.statusStrip_Main = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel_X = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_XValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Y = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_YValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Space01 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropX = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropXValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropY = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropYValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropWidth = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropWidthValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropHeight = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_CropHeightValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Space02 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Width = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_WidthValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Height = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_HeightValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Space03 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_DisplayWidth = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_DisplayWidthValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_DisplayHeight = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_DisplayHeightValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Space04 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Ratio = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_RatioValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).BeginInit();
            this.statusStrip_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip_Main
            // 
            this.menuStrip_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_File,
            this.ToolStripMenuItem_Help});
            this.menuStrip_Main.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_Main.Name = "menuStrip_Main";
            this.menuStrip_Main.Size = new System.Drawing.Size(884, 24);
            this.menuStrip_Main.TabIndex = 0;
            this.menuStrip_Main.Text = "menuStrip1";
            // 
            // ToolStripMenuItem_File
            // 
            this.ToolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_Open,
            this.ToolStripMenuItem_Save,
            this.ToolStripMenuItem_Exit});
            this.ToolStripMenuItem_File.Name = "ToolStripMenuItem_File";
            this.ToolStripMenuItem_File.Size = new System.Drawing.Size(37, 20);
            this.ToolStripMenuItem_File.Text = "File";
            // 
            // ToolStripMenuItem_Open
            // 
            this.ToolStripMenuItem_Open.Name = "ToolStripMenuItem_Open";
            this.ToolStripMenuItem_Open.Size = new System.Drawing.Size(103, 22);
            this.ToolStripMenuItem_Open.Text = "Open";
            this.ToolStripMenuItem_Open.Click += new System.EventHandler(this.ToolStripMenuItem_Open_Click);
            // 
            // ToolStripMenuItem_Save
            // 
            this.ToolStripMenuItem_Save.Name = "ToolStripMenuItem_Save";
            this.ToolStripMenuItem_Save.Size = new System.Drawing.Size(103, 22);
            this.ToolStripMenuItem_Save.Text = "Save";
            this.ToolStripMenuItem_Save.Click += new System.EventHandler(this.ToolStripMenuItem_Save_Click);
            // 
            // ToolStripMenuItem_Exit
            // 
            this.ToolStripMenuItem_Exit.Name = "ToolStripMenuItem_Exit";
            this.ToolStripMenuItem_Exit.Size = new System.Drawing.Size(103, 22);
            this.ToolStripMenuItem_Exit.Text = "Exit";
            this.ToolStripMenuItem_Exit.Click += new System.EventHandler(this.ToolStripMenuItem_Exit_Click);
            // 
            // ToolStripMenuItem_Help
            // 
            this.ToolStripMenuItem_Help.Name = "ToolStripMenuItem_Help";
            this.ToolStripMenuItem_Help.Size = new System.Drawing.Size(44, 20);
            this.ToolStripMenuItem_Help.Text = "Help";
            this.ToolStripMenuItem_Help.Click += new System.EventHandler(this.ToolStripMenuItem_Help_Click);
            // 
            // pictureBox_Main
            // 
            this.pictureBox_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Main.BackColor = System.Drawing.Color.Black;
            this.pictureBox_Main.Location = new System.Drawing.Point(0, 27);
            this.pictureBox_Main.Name = "pictureBox_Main";
            this.pictureBox_Main.Size = new System.Drawing.Size(884, 377);
            this.pictureBox_Main.TabIndex = 1;
            this.pictureBox_Main.TabStop = false;
            this.pictureBox_Main.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Main_Paint);
            this.pictureBox_Main.DoubleClick += new System.EventHandler(this.pictureBox_Main_DoubleClick);
            this.pictureBox_Main.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseDown);
            this.pictureBox_Main.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseMove);
            this.pictureBox_Main.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Main_MouseUp);
            // 
            // button_Resize
            // 
            this.button_Resize.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_Resize.Location = new System.Drawing.Point(201, 410);
            this.button_Resize.Name = "button_Resize";
            this.button_Resize.Size = new System.Drawing.Size(75, 23);
            this.button_Resize.TabIndex = 2;
            this.button_Resize.Text = "Resize H";
            this.button_Resize.UseVisualStyleBackColor = true;
            this.button_Resize.Click += new System.EventHandler(this.button_Resize_Click);
            // 
            // button_Crop
            // 
            this.button_Crop.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_Crop.Location = new System.Drawing.Point(282, 410);
            this.button_Crop.Name = "button_Crop";
            this.button_Crop.Size = new System.Drawing.Size(75, 23);
            this.button_Crop.TabIndex = 2;
            this.button_Crop.Text = "Crop";
            this.button_Crop.UseVisualStyleBackColor = true;
            this.button_Crop.Click += new System.EventHandler(this.button_Crop_Click);
            // 
            // button_LeftRotate
            // 
            this.button_LeftRotate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_LeftRotate.Location = new System.Drawing.Point(363, 410);
            this.button_LeftRotate.Name = "button_LeftRotate";
            this.button_LeftRotate.Size = new System.Drawing.Size(75, 23);
            this.button_LeftRotate.TabIndex = 2;
            this.button_LeftRotate.Text = "LeftRotate";
            this.button_LeftRotate.UseVisualStyleBackColor = true;
            this.button_LeftRotate.Click += new System.EventHandler(this.button_LeftRotate_Click);
            // 
            // button_RightRotate
            // 
            this.button_RightRotate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_RightRotate.Location = new System.Drawing.Point(444, 410);
            this.button_RightRotate.Name = "button_RightRotate";
            this.button_RightRotate.Size = new System.Drawing.Size(75, 23);
            this.button_RightRotate.TabIndex = 2;
            this.button_RightRotate.Text = "RightRotate";
            this.button_RightRotate.UseVisualStyleBackColor = true;
            this.button_RightRotate.Click += new System.EventHandler(this.button_RightRotate_Click);
            // 
            // button_WaterMask
            // 
            this.button_WaterMask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_WaterMask.Location = new System.Drawing.Point(525, 410);
            this.button_WaterMask.Name = "button_WaterMask";
            this.button_WaterMask.Size = new System.Drawing.Size(75, 23);
            this.button_WaterMask.TabIndex = 2;
            this.button_WaterMask.Text = "WaterMask";
            this.button_WaterMask.UseVisualStyleBackColor = true;
            this.button_WaterMask.Click += new System.EventHandler(this.button_WaterMask_Click);
            // 
            // button_TestMask
            // 
            this.button_TestMask.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_TestMask.Location = new System.Drawing.Point(606, 410);
            this.button_TestMask.Name = "button_TestMask";
            this.button_TestMask.Size = new System.Drawing.Size(75, 23);
            this.button_TestMask.TabIndex = 2;
            this.button_TestMask.Text = "TextMask";
            this.button_TestMask.UseVisualStyleBackColor = true;
            this.button_TestMask.Click += new System.EventHandler(this.button_TextMask_Click);
            // 
            // statusStrip_Main
            // 
            this.statusStrip_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_X,
            this.toolStripStatusLabel_XValue,
            this.toolStripStatusLabel_Y,
            this.toolStripStatusLabel_YValue,
            this.toolStripStatusLabel_Space01,
            this.toolStripStatusLabel_CropX,
            this.toolStripStatusLabel_CropXValue,
            this.toolStripStatusLabel_CropY,
            this.toolStripStatusLabel_CropYValue,
            this.toolStripStatusLabel_CropWidth,
            this.toolStripStatusLabel_CropWidthValue,
            this.toolStripStatusLabel_CropHeight,
            this.toolStripStatusLabel_CropHeightValue,
            this.toolStripStatusLabel_Space02,
            this.toolStripStatusLabel_Width,
            this.toolStripStatusLabel_WidthValue,
            this.toolStripStatusLabel_Height,
            this.toolStripStatusLabel_HeightValue,
            this.toolStripStatusLabel_Space03,
            this.toolStripStatusLabel_DisplayWidth,
            this.toolStripStatusLabel_DisplayWidthValue,
            this.toolStripStatusLabel_DisplayHeight,
            this.toolStripStatusLabel_DisplayHeightValue,
            this.toolStripStatusLabel_Space04,
            this.toolStripStatusLabel_Ratio,
            this.toolStripStatusLabel_RatioValue});
            this.statusStrip_Main.Location = new System.Drawing.Point(0, 439);
            this.statusStrip_Main.Name = "statusStrip_Main";
            this.statusStrip_Main.Size = new System.Drawing.Size(884, 22);
            this.statusStrip_Main.TabIndex = 3;
            // 
            // toolStripStatusLabel_X
            // 
            this.toolStripStatusLabel_X.Name = "toolStripStatusLabel_X";
            this.toolStripStatusLabel_X.Size = new System.Drawing.Size(17, 17);
            this.toolStripStatusLabel_X.Text = "X:";
            // 
            // toolStripStatusLabel_XValue
            // 
            this.toolStripStatusLabel_XValue.Name = "toolStripStatusLabel_XValue";
            this.toolStripStatusLabel_XValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_XValue.Text = "0";
            // 
            // toolStripStatusLabel_Y
            // 
            this.toolStripStatusLabel_Y.Name = "toolStripStatusLabel_Y";
            this.toolStripStatusLabel_Y.Size = new System.Drawing.Size(17, 17);
            this.toolStripStatusLabel_Y.Text = "Y:";
            // 
            // toolStripStatusLabel_YValue
            // 
            this.toolStripStatusLabel_YValue.Name = "toolStripStatusLabel_YValue";
            this.toolStripStatusLabel_YValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_YValue.Text = "0";
            // 
            // toolStripStatusLabel_Space01
            // 
            this.toolStripStatusLabel_Space01.Name = "toolStripStatusLabel_Space01";
            this.toolStripStatusLabel_Space01.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel_Space01.Size = new System.Drawing.Size(34, 17);
            this.toolStripStatusLabel_Space01.Spring = true;
            // 
            // toolStripStatusLabel_CropX
            // 
            this.toolStripStatusLabel_CropX.Name = "toolStripStatusLabel_CropX";
            this.toolStripStatusLabel_CropX.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel_CropX.Text = "Crop X:";
            // 
            // toolStripStatusLabel_CropXValue
            // 
            this.toolStripStatusLabel_CropXValue.Name = "toolStripStatusLabel_CropXValue";
            this.toolStripStatusLabel_CropXValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_CropXValue.Text = "0";
            // 
            // toolStripStatusLabel_CropY
            // 
            this.toolStripStatusLabel_CropY.Name = "toolStripStatusLabel_CropY";
            this.toolStripStatusLabel_CropY.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel_CropY.Text = "Crop Y:";
            // 
            // toolStripStatusLabel_CropYValue
            // 
            this.toolStripStatusLabel_CropYValue.Name = "toolStripStatusLabel_CropYValue";
            this.toolStripStatusLabel_CropYValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_CropYValue.Text = "0";
            // 
            // toolStripStatusLabel_CropWidth
            // 
            this.toolStripStatusLabel_CropWidth.Name = "toolStripStatusLabel_CropWidth";
            this.toolStripStatusLabel_CropWidth.Size = new System.Drawing.Size(71, 17);
            this.toolStripStatusLabel_CropWidth.Text = "Crop Width:";
            // 
            // toolStripStatusLabel_CropWidthValue
            // 
            this.toolStripStatusLabel_CropWidthValue.Name = "toolStripStatusLabel_CropWidthValue";
            this.toolStripStatusLabel_CropWidthValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_CropWidthValue.Text = "0";
            // 
            // toolStripStatusLabel_CropHeight
            // 
            this.toolStripStatusLabel_CropHeight.Name = "toolStripStatusLabel_CropHeight";
            this.toolStripStatusLabel_CropHeight.Size = new System.Drawing.Size(75, 17);
            this.toolStripStatusLabel_CropHeight.Text = "Crop Height:";
            // 
            // toolStripStatusLabel_CropHeightValue
            // 
            this.toolStripStatusLabel_CropHeightValue.Name = "toolStripStatusLabel_CropHeightValue";
            this.toolStripStatusLabel_CropHeightValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_CropHeightValue.Text = "0";
            // 
            // toolStripStatusLabel_Space02
            // 
            this.toolStripStatusLabel_Space02.Name = "toolStripStatusLabel_Space02";
            this.toolStripStatusLabel_Space02.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel_Space02.Size = new System.Drawing.Size(34, 17);
            this.toolStripStatusLabel_Space02.Spring = true;
            // 
            // toolStripStatusLabel_Width
            // 
            this.toolStripStatusLabel_Width.Name = "toolStripStatusLabel_Width";
            this.toolStripStatusLabel_Width.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel_Width.Text = "Width:";
            // 
            // toolStripStatusLabel_WidthValue
            // 
            this.toolStripStatusLabel_WidthValue.Name = "toolStripStatusLabel_WidthValue";
            this.toolStripStatusLabel_WidthValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_WidthValue.Text = "0";
            // 
            // toolStripStatusLabel_Height
            // 
            this.toolStripStatusLabel_Height.Name = "toolStripStatusLabel_Height";
            this.toolStripStatusLabel_Height.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel_Height.Text = "Height:";
            // 
            // toolStripStatusLabel_HeightValue
            // 
            this.toolStripStatusLabel_HeightValue.Name = "toolStripStatusLabel_HeightValue";
            this.toolStripStatusLabel_HeightValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_HeightValue.Text = "0";
            // 
            // toolStripStatusLabel_Space03
            // 
            this.toolStripStatusLabel_Space03.Name = "toolStripStatusLabel_Space03";
            this.toolStripStatusLabel_Space03.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel_Space03.Size = new System.Drawing.Size(34, 17);
            this.toolStripStatusLabel_Space03.Spring = true;
            // 
            // toolStripStatusLabel_DisplayWidth
            // 
            this.toolStripStatusLabel_DisplayWidth.Name = "toolStripStatusLabel_DisplayWidth";
            this.toolStripStatusLabel_DisplayWidth.Size = new System.Drawing.Size(83, 17);
            this.toolStripStatusLabel_DisplayWidth.Text = "Display Width:";
            // 
            // toolStripStatusLabel_DisplayWidthValue
            // 
            this.toolStripStatusLabel_DisplayWidthValue.Name = "toolStripStatusLabel_DisplayWidthValue";
            this.toolStripStatusLabel_DisplayWidthValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_DisplayWidthValue.Text = "0";
            // 
            // toolStripStatusLabel_DisplayHeight
            // 
            this.toolStripStatusLabel_DisplayHeight.Name = "toolStripStatusLabel_DisplayHeight";
            this.toolStripStatusLabel_DisplayHeight.Size = new System.Drawing.Size(87, 17);
            this.toolStripStatusLabel_DisplayHeight.Text = "Display Height:";
            // 
            // toolStripStatusLabel_DisplayHeightValue
            // 
            this.toolStripStatusLabel_DisplayHeightValue.Name = "toolStripStatusLabel_DisplayHeightValue";
            this.toolStripStatusLabel_DisplayHeightValue.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel_DisplayHeightValue.Text = "0";
            // 
            // toolStripStatusLabel_Space04
            // 
            this.toolStripStatusLabel_Space04.Name = "toolStripStatusLabel_Space04";
            this.toolStripStatusLabel_Space04.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatusLabel_Space04.Size = new System.Drawing.Size(34, 17);
            this.toolStripStatusLabel_Space04.Spring = true;
            // 
            // toolStripStatusLabel_Ratio
            // 
            this.toolStripStatusLabel_Ratio.Name = "toolStripStatusLabel_Ratio";
            this.toolStripStatusLabel_Ratio.Size = new System.Drawing.Size(37, 17);
            this.toolStripStatusLabel_Ratio.Text = "Ratio:";
            // 
            // toolStripStatusLabel_RatioValue
            // 
            this.toolStripStatusLabel_RatioValue.Name = "toolStripStatusLabel_RatioValue";
            this.toolStripStatusLabel_RatioValue.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel_RatioValue.Text = "100%";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(120, 410);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Resize W";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Resize_W_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.statusStrip_Main);
            this.Controls.Add(this.button_TestMask);
            this.Controls.Add(this.button_WaterMask);
            this.Controls.Add(this.button_RightRotate);
            this.Controls.Add(this.button_LeftRotate);
            this.Controls.Add(this.button_Crop);
            this.Controls.Add(this.button_Resize);
            this.Controls.Add(this.pictureBox_Main);
            this.Controls.Add(this.menuStrip_Main);
            this.MainMenuStrip = this.menuStrip_Main;
            this.Name = "Form_Main";
            this.Text = "Image Editing";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.Resize += new System.EventHandler(this.Form_Main_Resize);
            this.menuStrip_Main.ResumeLayout(false);
            this.menuStrip_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).EndInit();
            this.statusStrip_Main.ResumeLayout(false);
            this.statusStrip_Main.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip_Main;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Open;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Save;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Exit;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_Help;
        private System.Windows.Forms.PictureBox pictureBox_Main;
        private System.Windows.Forms.Button button_Resize;
        private System.Windows.Forms.Button button_Crop;
        private System.Windows.Forms.Button button_LeftRotate;
        private System.Windows.Forms.Button button_RightRotate;
        private System.Windows.Forms.Button button_WaterMask;
        private System.Windows.Forms.Button button_TestMask;
        private System.Windows.Forms.StatusStrip statusStrip_Main;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropX;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropXValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropY;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropYValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropWidth;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropWidthValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropHeight;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_CropHeightValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Width;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_WidthValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Height;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_HeightValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_DisplayWidth;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_DisplayWidthValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_DisplayHeight;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_DisplayHeightValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Ratio;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Space02;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_RatioValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_X;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_XValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Y;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_YValue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Space01;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Space03;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Space04;
        private System.Windows.Forms.Button button1;
    }
}

