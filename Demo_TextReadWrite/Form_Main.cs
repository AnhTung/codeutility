﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeUtility;

namespace Demo_TextReadWrite
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {

        }

        private void ToolStripMenuItem_Open_Click(object sender, EventArgs e)
        {
            //1. Khai báo biến chứa URL
            string url = string.Empty;

            //2. Lấy URL từ hộp thoại
            string title = "Mời chọn 1 file text";
            string filter = "Text file|*.txt|All file|*.*";
            url = DialogUtility.FileDialog(title, filter);

            if (url == string.Empty)
            {
                MessageBox.Show("Bạn chưa chọn file.");
                return;
            }

            //3. Đọc nội dung trong file 
            Exception ex = null;
            string text = FileUtility.ReadFile(url, ref ex);

            //4. Kiểm tra lỗi nếu có
            if(ex != null)
            {
                MessageBox.Show("Có lỗi trong quá trình đọc file. " + ex.Message);
                text = string.Empty;
            }

            //4. Hiện nội dung lên màn hình
            textBox_Value.Text = text;

            //5. Kết thúc
            return;
        }

        private void ToolStripMenuItem_Save_Click(object sender, EventArgs e)
        {
            //1. Khai báo biến chứa URL
            string url = string.Empty;

            //2. Lấy URL từ hộp thoại
            string title = "Mời chọn 1 file text để lưu giá trị";
            string filter = "Text file|*.txt|All file|*.*";
            url = DialogUtility.FileSaveDialog(title, filter);

            if (url == string.Empty)
            {
                MessageBox.Show("Bạn chưa chọn file để lưu.");
                return;
            }

            //3. Ghi nội dung vào file
            string text = textBox_Value.Text;
            Exception ex = null;
            bool result = FileUtility.WriteFile(url, text, ref ex);

            //4. Kiểm tra lỗi
            if(ex != null)
            {
                MessageBox.Show("Có lỗi trong quá trình lưu file. " + ex.Message);
                return;
            }

            //5. Kết thúc
            return;
        }

        private void ToolStripMenuItem_Close_Click(object sender, EventArgs e)
        {
            bool isClose = false;
            isClose = DialogUtility.Confirm("Nhắc nhở", "Bạn có muốn thoát không?");

            if (isClose)
                this.Close();
            else
                return;
        }
    }
}
